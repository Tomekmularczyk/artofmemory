package com.controller.tomaszm;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;

public class StoperController implements Initializable {
	@FXML
	private ComboBox<Integer> comboBoxMinutes, comboBoxHours, comboBoxSeconds;
	@FXML
	private Button buttSTART;
	@FXML
	private CheckBox checkTestWhenFinished, checkGenerateNewNumbers;
	
	
	public CheckBox getCheckTestWhenFinished(){
		return checkTestWhenFinished;
	}
	public CheckBox getCheckGenerateNewNumbers(){
		return checkGenerateNewNumbers;
	}
	
	private final ObservableList<Integer> dataHours = FXCollections.observableArrayList(
			0,1,2,3,4,5,6,7,8,9);
	private final ObservableList<Integer> dataMinutes = FXCollections.observableArrayList(
			0,1,2,3,4,5,6,7,8,9,
			10,11,12,13,14,15,16,17,18,19,
			20,21,22,23,24,25,26,27,28,29,
			30,31,32,33,34,35,36,37,38,39,
			40,41,42,43,44,45,46,47,48,49,
			50,51,52,53,54,55,56,57,58,59);
	private final ObservableList<Integer> dataSeconds = FXCollections.observableArrayList(
			0,1,2,3,4,5,6,7,8,9,
			10,11,12,13,14,15,16,17,18,19,
			20,21,22,23,24,25,26,27,28,29,
			30,31,32,33,34,35,36,37,38,39,
			40,41,42,43,44,45,46,47,48,49,
			50,51,52,53,54,55,56,57,58,59);

	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		comboBoxHours.setItems(dataHours);
		comboBoxMinutes.setItems(dataMinutes);
		comboBoxSeconds.setItems(dataSeconds);
		comboBoxHours.setValue(dataHours.get(0));
		comboBoxMinutes.setValue(dataMinutes.get(5));
		comboBoxSeconds.setValue(dataSeconds.get(0));
	}


	public ComboBox<Integer> getComboBoxMinutes() {
		return comboBoxMinutes;
	}
	public ComboBox<Integer> getComboBoxHours() {
		return comboBoxHours;
	}
	public ComboBox<Integer> getComboBoxSeconds() {
		return comboBoxSeconds;
	}
	public Button getButtSTART() {
		return buttSTART;
	}
}
