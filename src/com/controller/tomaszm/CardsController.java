package com.controller.tomaszm;

import com.model.tomaszm.CONSTANTS;
import com.model.tomaszm.ChangeTheRoot;
import com.model.tomaszm.Controller;
import com.model.tomaszm.INFO;
import com.model.tomaszm.Main;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import org.controlsfx.control.PopOver;
import org.controlsfx.control.PopOver.ArrowLocation;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;

public class CardsController extends Controller implements Initializable {

    private boolean isOnTestMode = false;
    private TextArea textAreaSource;
    private Stage stageSource;
    private Scene sourceScene;
    private BorderPane sourcePane; //source window

    private PopOver popOverTimer;
    private BorderPane stoperPane;
    private StoperController stoperController;
    private CountdownController countdownController;
    private boolean isCounting = false;
    private StackPane countdownPane;

    private PopOver popOverAbout;

    private ArrayList<CardsTableController> cardsTableControllerList = new ArrayList<>();

    @FXML
    private StackPane stackPane; //top pane
    @FXML
    private BorderPane borderPane;
    @FXML
    private TabPane tabPane;
    @FXML
    private MenuItem menuItemTryMe, menuItemSource, menuItemCheck, menuItemStartAgain, menuItemSetStoper;
    @FXML
    private Menu menuDeck;
    @FXML
    private MenuBar menuBar;

    //---------- Menu Options ------------
    @FXML
    private void menuItemMainMenuAction(ActionEvent e) {
        hidePopOverAndStopCounting();
        stageSource.close();

        ChangeTheRoot change = new ChangeTheRoot();
        if (change.initialize(Main.primaryStage, false, CONSTANTS.MAIN_ROOT.directory, "ArtOfMemory", true) == false) {
            Dialogs.create().title("Error").masthead(null).message("Couldn't load the components!").showError();
        } else {
            change.doFadingTransition(borderPane, CONSTANTS.fadeOutMillis.value, CONSTANTS.fadeInMillis.value, true);
        }
    }

    @FXML
    private void menuItemTryMeAction(ActionEvent e) {
        if (tabPane.getTabs().size() > 0) {
            hidePopOverAndStopCounting();
            isOnTestMode = true;

            enableMenuItemsVisibility();

            for (int i = 0; i < cardsTableControllerList.size(); i++) {
                textAreaSource.appendText("DECK " + (i + 1) + "\n" + cardsTableControllerList.get(i).getCardsLayoutOrder() + "\n");
                cardsTableControllerList.get(i).setSplitPaneDevider(0.5f);
                cardsTableControllerList.get(i).test();
            }
            ObservableList<Tab> tabList = tabPane.getTabs();
            for (int i = 0; i < tabList.size(); i++) {
                tabList.get(i).setClosable(false);
            }
        } else {
            Dialogs.create().title("Info").masthead(null).message("There's nothing to check :)").showInformation();
        }
    }

    private void hidePopOverAndStopCounting() {
        popOverTimer.hide();
        popOverAbout.hide();
        stopCounting(); //in case its open thread
    }

    private void enableMenuItemsVisibility() {
        menuItemTryMe.setVisible(false);
        menuItemSetStoper.setVisible(false);
        menuItemSource.setVisible(true);
        menuItemCheck.setVisible(true);
        menuItemStartAgain.setVisible(true);
        menuDeck.setDisable(true);
    }

    @FXML
    private void menuItemStartAgainAction(ActionEvent e) {
        stageSource.close();
        isOnTestMode = false;

        disableMenuItemsVisibility();

        for (int i = 0; i < cardsTableControllerList.size(); i++) {
            cardsTableControllerList.get(i).finishTest();
            cardsTableControllerList.get(i).setSplitPaneDevider(1.0f);
        }
        ObservableList<Tab> tabList = tabPane.getTabs();
        for (int i = 0; i < tabList.size(); i++) {
            tabList.get(i).setClosable(true);
        }
    }

    private void disableMenuItemsVisibility() {
        menuItemTryMe.setVisible(true);
        menuItemSetStoper.setVisible(true);
        menuItemSource.setVisible(false);
        menuItemCheck.setVisible(false);
        menuItemStartAgain.setVisible(false);
        menuDeck.setDisable(false);
    }

    @FXML
    private void menuItemSetStoperAction(ActionEvent e) {
        popOverTimer.show(menuBar);
    }

    @FXML
    private void menuItemCheckAction(ActionEvent e) {
        isOnTestMode = false;
        for (int i = 0; i < cardsTableControllerList.size(); i++) {
            cardsTableControllerList.get(i).checkDeck();
        }

    }

    @FXML
    private void menuItemCloseAction(ActionEvent e) {
        Platform.exit();
    }

    @FXML
    private void menuItemAboutAction(ActionEvent e) {
        popOverAbout.show(menuBar);
    }

    @FXML
    private void menuItemSourceFileAction(ActionEvent e) {
        if (isOnTestMode) {
            Action response = Dialogs.create()
                    .owner(Main.primaryStage)
                    .title("Confirm")
                    .message("You haven't finished yet, are You sure?")
                    .actions(Dialog.Actions.OK, Dialog.Actions.CANCEL)
                    .showConfirm();
            if (response == Dialog.Actions.OK) {
                stageSource.show();
            } else {
                // ... user chose CANCEL, or closed the dialog
            }
        } else {
            stageSource.show();
        }
    }

    //---------- Menu Deck ------------
    @FXML
    private void menuItemNewDeckAction(ActionEvent e) {
        addNewCardsTable();
    }

    @FXML
    private void menuItemShuffleAction(ActionEvent e) {
        int tabNr = tabPane.getSelectionModel().getSelectedIndex();
        cardsTableControllerList.get(tabNr).shuffle();
    }

    @FXML
    private void menuItemShuffleAllAction(ActionEvent e) {
        for (int i = 0; i < cardsTableControllerList.size(); i++) {
            cardsTableControllerList.get(i).shuffle();
        }
    }

    @FXML
    private void menuItemCloseAllAction(ActionEvent e) {
        stopCounting(); //in case its open thread
        cardsTableControllerList.clear();
        tabPane.getTabs().clear();
    }

    //----------------- this class methods
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        addNewCardsTable();
        menuItemTryMe.setStyle("-fx-text-fill: rgb(230,77,77)");

        initializeStoperAndCountdown();

        tabPane.setTabMinWidth(60.0);
        tabPane.setBackground(new Background(new BackgroundImage(new Image(CONSTANTS.TAB_PANE_BACKGROUND.directory),
                BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT,
                BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT)));

        Main.primaryStage.setOnCloseRequest(e -> {		//unfortunatelly we have to do this, because If popOver is visible
            popOverTimer.hide(new Duration(0));      //and user closes application popover causes the problem		
            popOverAbout.hide(new Duration(0));
        });

        initPopOver();
        initSourceWindow();
    }

    private void initPopOver() {
        popOverTimer = new PopOver(stoperPane);
        popOverTimer.setArrowLocation(ArrowLocation.RIGHT_TOP);
        popOverTimer.setDetachedTitle("Timer");
        popOverAbout = new PopOver(INFO.getCardsInfo());
        popOverAbout.setDetachedTitle("Cards info");
    }

    private void initializeStoperAndCountdown() {
        FXMLLoader loader;
        //------------ Stoper --------------
        try {
            loader = new FXMLLoader(getClass().getResource(CONSTANTS.STOPER_ROOT.directory));
            stoperPane = loader.load();
            stoperController = loader.getController();
            stoperController.getButtSTART().setOnAction(e -> {
                popOverTimer.setContentNode(countdownPane);
                if (stoperController.getCheckGenerateNewNumbers().isSelected()) {
                    menuItemShuffleAllAction(e);
                }
                countdownController.initCountdownController(stoperController.getComboBoxHours().getValue(),
                        stoperController.getComboBoxMinutes().getValue(),
                        stoperController.getComboBoxSeconds().getValue());
                countdownController.START();
                isCounting = true;
            });
            stoperController.getCheckGenerateNewNumbers().setText("Shuffle every deck");
            stoperController.getCheckTestWhenFinished().setText("Test me when finished");
        } catch (IOException e) {
            Dialogs.create().title("Exception").masthead(null).message("Couldnt load the stoper fxml!").showError();
            e.printStackTrace();
        }
        //------------ Countdown pane --------------
        try {
            loader = new FXMLLoader(getClass().getResource(CONSTANTS.COUNTDOWN_ROOT.directory));
            countdownPane = loader.load();
            countdownController = loader.getController();
            countdownController.getButtSTOP().setOnAction(e -> {
                popOverTimer.setContentNode(stoperPane);
                countdownController.STOP();
                isCounting = false;
            });
            countdownController.setOnAction(e -> {   //when countdown comes to end
                popOverTimer.setContentNode(stoperPane);
                if (stoperController.getCheckTestWhenFinished().isSelected()) {
                    menuItemTryMeAction(e);
                }
            });
        } catch (IOException e) {
            Dialogs.create().title("Exception").masthead(null).message("Couldnt load the countdown fxml!").showError();
            e.printStackTrace();
        }
    }

    private void stopCounting() {
        if (isCounting == true) {
            countdownController.STOP();
        }
    }

    @Override
    public Pane getMainPane() {
        return stackPane;
    }

    private void initSourceWindow() {
        textAreaSource = new TextArea();
        textAreaSource.setEditable(false);
        textAreaSource.setFont(new Font(java.awt.Font.MONOSPACED, 14));

        sourcePane = new BorderPane(textAreaSource);
        sourcePane.setPrefSize(380, 180);
        stageSource = new Stage();
        stageSource.getIcons().add(new Image(CONSTANTS.APPLICATION_ICON.directory));
        sourceScene = new Scene(sourcePane);
        stageSource.setScene(sourceScene);
        stageSource.initOwner(Main.primaryStage);
        stageSource.initModality(Modality.NONE);
        stageSource.initStyle(StageStyle.UTILITY);
        stageSource.setTitle("Source cards");
    }

    private void addNewCardsTable() {
        FXMLLoader loader;
        Pane cardsTablePane;
        Tab tab = new Tab("Deck " + (cardsTableControllerList.size() + 1));
        tab.setStyle("-fx-background-color: rgba(196,117,35,0.17)");
        //------------ CarsTable --------------
        try {
            loader = new FXMLLoader(getClass().getResource(CONSTANTS.CARDS_TABLE_ROOT.directory));
            cardsTablePane = loader.load();
            tab.setContent(cardsTablePane);
            tab.setId(String.valueOf(cardsTableControllerList.size()));
            tab.setOnClosed(new EventHandler<Event>() {
                @Override
                public void handle(Event event) {
                    int tabNr = Integer.valueOf(((Tab) event.getSource()).getId());
                    cardsTableControllerList.remove(tabNr);
                    refreshTabsNameAndID();
                    if (tabPane.getTabs().size() < 1) {
                        stopCounting(); //in case its open thread
                    }
                }
            });

            tabPane.getTabs().add(tab);
            cardsTableControllerList.add(loader.getController());

        } catch (IOException e) {
            Dialogs.create().title("Exception").masthead(null).message("Couldnt load the CardsTable fxml!").showError();
            e.printStackTrace();
        }
    }

    private void refreshTabsNameAndID() { //we will use it when user deletes a tab
        for (int i = 0; i < tabPane.getTabs().size(); i++) {
            tabPane.getTabs().get(i).setText("Deck " + (i + 1));
            tabPane.getTabs().get(i).setId(String.valueOf(i));
        }
    }
}
