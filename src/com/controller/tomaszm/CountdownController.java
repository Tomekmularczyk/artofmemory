package com.controller.tomaszm;

import com.model.tomaszm.Controller;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;

public class CountdownController extends Controller implements Initializable {

    @FXML
    private StackPane stackPane;
    @FXML
    private Label labTime;
    @FXML
    private Button buttSTOP;

    private Timer timer;
    private String sFormat = "%02d"; //so its 01:04:09
    private int iHours = 0,
            iMinutes = 1,
            iSeconds = 10;
    private ArrayList<EventHandler<ActionEvent>> listeners = new ArrayList<>();

    public void initCountdownController(int iHours, int iMinutes, int iSeconds) {
        this.iHours = iHours;
        this.iMinutes = iMinutes;
        this.iSeconds = iSeconds;

        buttSTOP.setId("stoperButton");
    }

    public Button getButtSTOP() {
        return buttSTOP;
    }

    ;
	@Override
    public void initialize(URL location, ResourceBundle resources) {
        buttSTOP.setId("stoperButton");
        buttSTOP.setOnAction(e -> {
            buttSTOPAction(e);
        });

        InputStream is = this.getClass().getResourceAsStream("/DS-DIGIT.TTF");
        Font uniFont = Font.loadFont(is, 30);
        labTime.setFont(uniFont);
    }

    public void setOnAction(EventHandler<ActionEvent> listener) {
        listeners.add(listener);
    }

    public void START() {
        timer = new Timer(true); //set it as a deamon
        timer.schedule(new MyTimer(), 0, 1000);
    }

    public void STOP() {
        timer.cancel();
    }

    private void buttSTOPAction(ActionEvent e) {
        STOP();
    }

    public class MyTimer extends TimerTask {

        @Override
        public void run() {
            StringBuilder time = new StringBuilder();
            time.append(String.format(sFormat, iHours) + ":");
            time.append(String.format(sFormat, iMinutes) + ":");
            time.append(String.format(sFormat, iSeconds));

            boolean timeOut = iSeconds + iMinutes + iHours == 0;
            Platform.runLater(() -> {
                labTime.setText(time.toString());
                if (timeOut) {
                    for (EventHandler<ActionEvent> l : listeners) {
                        l.handle(new ActionEvent());
                    }
                }
            });

            if (iSeconds < 1) {
                if (iMinutes < 1) {
                    if (iHours < 1) {
                        this.cancel();
                    } else {
                        --iHours;
                        iMinutes = 59;
                        iSeconds = 59;
                    }
                } else {
                    --iMinutes;
                    iSeconds = 59;
                }
            } else {
                --iSeconds;
            }

        }
    }

    @Override
    public Pane getMainPane() {
        return stackPane;
    }

}
