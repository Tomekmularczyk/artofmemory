package com.controller.tomaszm;

import com.model.tomaszm.CONSTANTS;
import com.model.tomaszm.ChangeTheRoot;
import com.model.tomaszm.Controller;
import com.model.tomaszm.INFO;
import com.model.tomaszm.Main;
import com.model.tomaszm.NumberGeneratorModel;
import java.net.URL;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;
import javafx.util.converter.NumberStringConverter;
import org.controlsfx.control.PopOver;
import org.controlsfx.dialog.Dialogs;

public class SpokenNumbersController extends Controller implements Initializable {

    private enum TYPE {
        DECIMAL, BINARY
    }
    TYPE type = TYPE.DECIMAL;
    private Thread thread = new Thread();
    private PlayDigits playDigits;
    private final StringBuilder language = new StringBuilder("/PL.");
    private final StringBuilder lastCountdownData = new StringBuilder();
    private PopOver popOverAbout;

    private final ObservableList<String> dataInterval = FXCollections.observableArrayList(
            "0.4 sec", "0.5 sec", "0.6 sec", "0.7 sec", "0.8 sec", "0.9 sec", "1.0 sec", "1.1 sec", "1.2 sec", "1.3 sec", "1.4 sec", "1.5 sec", "1.6 sec", "1.7 sec", "1.8 sec", "1.9 sec",
            "2.0 sec", "2.1 sec", "2.2 sec", "2.3 sec", "2.4 sec", "2.5 sec", "2.6 sec", "2.7 sec", "2.8 sec", "2.9 sec", "3.0 sec", "3.1 sec", "3.2 sec", "3.3 sec", "3.4 sec", "3.5 sec", "3.6 sec", "3.7 sec", "3.8 sec", "3.9 sec",
            "4.0 sec", "4.1 sec", "4.2 sec", "4.3 sec", "4.4 sec", "4.5 sec", "4.6 sec", "4.7 sec", "4.8 sec", "4.9 sec", "5.0 sec", "5.1 sec", "5.2 sec", "5.3 sec", "5.4 sec", "5.5 sec", "5.6 sec", "5.7 sec", "5.8 sec", "5.9 sec",
            "6.0 sec", "6.1 sec", "6.2 sec", "6.3 sec", "6.4 sec", "6.5 sec", "6.6 sec", "6.7 sec", "6.8 sec", "6.9 sec", "7.0 sec", "7.1 sec", "7.2 sec", "7.3 sec", "7.4 sec", "7.5 sec", "7.6 sec", "7.7 sec", "7.8 sec", "7.9 sec",
            "8.0 sec", "8.1 sec", "8.2 sec", "8.3 sec", "8.4 sec", "8.5 sec", "8.6 sec", "8.7 sec", "8.8 sec", "8.9 sec", "9.0 sec", "9.1 sec", "9.2 sec", "9.3 sec", "9.4 sec", "9.5 sec", "9.6 sec", "9.7 sec", "9.8 sec", "9.9 sec", "10.0 seconds");
    private final ObservableList<String> dataExtraInterval = FXCollections.observableArrayList(
            "1 sec", "2 sec", "3 sec", "4 sec", "5 sec", "6 sec", "7 sec", "8 sec", "9 sec", "10 sec", "11 sec", "12 sec", "13 sec", "14 sec", "15 sec", "16 sec", "17 sec", "18 sec", "19 sec", "20 sec",
            "30 sec", "31 sec", "32 sec", "33 sec", "34 sec", "35 sec", "36 sec", "37 sec", "38 sec", "39 sec", "40 sec", "41 sec", "42 sec", "43 sec", "44 sec", "45 sec", "46 sec", "47 sec", "48 sec", "49 sec", "50 sec", "51 sec", "52 sec", "53 sec", "54 sec", "55 sec", "56 sec", "57 sec", "58 sec", "59 sec", "60 sec");
    private final ObservableList<String> dataEach = FXCollections.observableArrayList(
            "2 digits", "3 digits", "4 digits", "5 digits", "6 digits", "7 digits", "8 digits", "9 digits", "10 digits", "11 digits", "12 digits", "13 digits", "14 digits", "15 digits", "16 digits", "17 digits", "18 digits", "19 digits", "20 digits",
            "30 digits", "31 digits", "32 digits", "33 digits", "34 digits", "35 digits", "36 digits", "37 digits", "38 digits", "39 digits", "40 digits", "41 digits", "42 digits", "43 digits", "44 digits", "45 digits", "46 digits", "47 digit", "48 digits", "49 digits", "50 digits", "51 digits", "52 digits", "53 digits", "54 digits", "55 digits", "56 digits", "57 digits", "58 digits", "59 digits", "60 digits");

    @FXML
    private ToggleButton toggbuttSTART;
    @FXML
    private ComboBox<String> comboboxInterval, comboboxExtraInterval, comboboxEach;
    @FXML
    private CheckBox checkExtra, checkTestMe;
    @FXML
    private MenuBar menuBar;
    @FXML
    private RadioMenuItem radiomenuPolish, radiomenuEnglish, radiomenuDeutsch, radiomenuChinesse,
            radiomenuDecimal, radiomenuBinary;
    @FXML
    private MenuItem menuItemTryMe;
    @FXML
    private Pane breaksPane;
    @FXML
    private Slider sliderCountdown;
    @FXML
    private Label labCountdown, labNumber;
    @FXML
    private BorderPane borderPane;

    @FXML
    private void selectLanguageAction(ActionEvent e) {
        Object node = e.getSource();

        if (node == radiomenuPolish) {
            language.replace(0, 4, "/PL.");
        } else if (node == radiomenuEnglish) {
            language.replace(0, 4, "/EN.");
        } else if (node == radiomenuDeutsch) {
            language.replace(0, 4, "/DE.");
        } else if (node == radiomenuChinesse) {
            language.replace(0, 4, "/CH.");
        }
    }

    @FXML
    private void selectTypeAction(ActionEvent e) {
        Object node = e.getSource();
        if (node == radiomenuBinary) {
            type = TYPE.BINARY;
        } else if (node == radiomenuDecimal) {
            type = TYPE.DECIMAL;
        }

    }

    @FXML
    private void toggbuttSTARTAction(ActionEvent e) {
        if (toggbuttSTART.isSelected()) {
            if (!thread.isAlive()) {
                playDigits = new PlayDigits();
                labNumber.setText("0");
                playDigits.addOnFinishListener(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        countdownFinishedOrStopped(event);
                    }
                });
                thread = new Thread(playDigits);
                thread.setDaemon(true);
                menuItemTryMe.setDisable(true);
                thread.start();
            }
            toggbuttSTART.setText("STOP");
        } else { //----- STOP
            if (thread.isAlive()) {
                playDigits.stop();
            }
            toggbuttSTART.setText("PLAY");
        }
    }

    @FXML
    private void checkExtraAction(ActionEvent e) {
        if (checkExtra.isSelected()) {
            breaksPane.setDisable(false);
        } else { //--- not selected
            breaksPane.setDisable(true);
        }
    }

    @FXML
    void buttPlusAction(ActionEvent e) {
        if (sliderCountdown.getMax() > sliderCountdown.getValue()) {
            sliderCountdown.increment();
        }
    }

    @FXML
    void buttMinusAction(ActionEvent e) {
        if (sliderCountdown.getMin() < sliderCountdown.getValue()) {
            sliderCountdown.decrement();
        }
    }

    @FXML
    private void menuItemCloseAction(ActionEvent e) {
        Platform.exit();
    }

    @FXML
    private void menuItemGoToMainMenuAction(ActionEvent e) {
        popOverAbout.hide();
        if (thread.isAlive()) {
            playDigits.stop();
            thread.interrupt();
        }

        ChangeTheRoot change = new ChangeTheRoot();

        if (!change.initialize(Main.primaryStage, false, CONSTANTS.MAIN_ROOT.directory, "ArtOfMemory", true)) {
            Dialogs.create().title("Error").masthead(null).message("Couldn't load the components!").showError();
        } else {
            change.doFadingTransition(borderPane, CONSTANTS.fadeOutMillis.value, CONSTANTS.fadeInMillis.value, true);
        }
    }

    @FXML
    private void menuItemTryMeAction(ActionEvent e) {
        popOverAbout.hide();
        if (lastCountdownData.length() > 0) {
            ChangeTheRoot change = new ChangeTheRoot();

            if (!change.initialize(Main.primaryStage, true, CONSTANTS.NUMBERS_TEST_SHEET_ROOT.directory, "Test sheet", true)) {
                Dialogs.create().title("Error").masthead(null).message("Can't load the components!").showError();
            } else {
                //getting how many rows and columns we need
                ((NumbersTestSheetController) change.getController()).initData(10, playDigits.getNumberOfDigits() / 10, lastCountdownData.toString());
                change.doFadingTransition(borderPane, CONSTANTS.fadeOutMillis.value, CONSTANTS.fadeInMillis.value);
            }
        } else {
            Dialogs.create().title("Exception").masthead(null).message("There is no numbers to check!").showInformation();
        }
    }

    @FXML
    private void menuItemAboutAction(ActionEvent e) {
        popOverAbout.show(menuBar);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initComboBoxes();
        initCountdownLabel();
        initBorderPaneBackground();
        initTooltip();
        initPopOver();
    }

    private void initComboBoxes() {
        toggbuttSTART.getStyleClass().add("togglebutton");
        comboboxInterval.setItems(dataInterval);
        comboboxInterval.setValue(dataInterval.get(6));
        comboboxEach.setItems(dataEach);
        comboboxEach.setValue(dataEach.get(4));
        comboboxExtraInterval.setItems(dataExtraInterval);
        comboboxExtraInterval.setValue(dataExtraInterval.get(4));
    }

    private void initCountdownLabel() {
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(0);
        labCountdown.textProperty().bindBidirectional(sliderCountdown.valueProperty(), new NumberStringConverter(nf));
    }

    private void initBorderPaneBackground() {
        borderPane.setBackground(new Background(new BackgroundImage(new Image(CONSTANTS.SPOKEN_NUMBERS_BACKGROUND.directory),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));
    }

    private void initTooltip() {
        INFO.tooltipCountdown.setOpacity(0.9);
        INFO.tooltipInterval.setOpacity(0.9);
        sliderCountdown.setTooltip(INFO.tooltipCountdown);
        comboboxInterval.setTooltip(INFO.tooltipInterval);
    }

    private void initPopOver() {
        popOverAbout = new PopOver(INFO.getSpokenNumbersInfo());
        popOverAbout.setDetachedTitle("Spoken numbers info");
        Main.primaryStage.setOnCloseRequest(e -> {
            popOverAbout.hide(new Duration(0));
        });
    }

    @Override
    public Pane getMainPane() {
        return borderPane;
    }

    private void countdownFinishedOrStopped(ActionEvent e) {
        thread.interrupt();
        menuItemTryMe.setDisable(false);
        if (checkTestMe.isSelected() == true) {
            menuItemTryMeAction(new ActionEvent());
        }
    }

    /**
     * **************************************
     */
    //-- commented code is used instead of MediaPlayer when we want .jar file to use sound files inside of jar.
    //-- however MediaPlayer is more stable than Player and its better for instalation package
    private class PlayDigits implements Runnable {

        private volatile boolean stop = false;
        private int numberOfDigits = 0;
        private int digitsToFinish;
        private ArrayList<EventHandler<ActionEvent>> listeners = new ArrayList<>();
        private TYPE thisType;

        private StringBuilder generatedDigits;
        //String media[] = new String[10];
        MediaPlayer media[] = new MediaPlayer[10];

        public PlayDigits() {
            generatedDigits = new StringBuilder();
            this.thisType = type;

            //--- we are loading sounds in particular language(its causing no offsync when we try to read sound every time)
            for (int i = 0; i < 10; ++i) {
                StringBuilder directory = new StringBuilder(language.toString());
                switch (i) {
                    case 0:
                        directory.append("Zero.mp3");
                        break;
                    case 1:
                        directory.append("One.mp3");
                        break;
                    case 2:
                        directory.append("Two.mp3");
                        break;
                    case 3:
                        directory.append("Three.mp3");
                        break;
                    case 4:
                        directory.append("Four.mp3");
                        break;
                    case 5:
                        directory.append("Five.mp3");
                        break;
                    case 6:
                        directory.append("Six.mp3");
                        break;
                    case 7:
                        directory.append("Seven.mp3");
                        break;
                    case 8:
                        directory.append("Eight.mp3");
                        break;
                    case 9:
                        directory.append("Nine.mp3");
                        break;
                    default:
                        break;
                }

                URL path = getClass().getResource(directory.toString());
                media[i] = new MediaPlayer(new Media(path.toString()));
            }

            digitsToFinish = (int) Math.round(sliderCountdown.getValue());
        }

        public void addOnFinishListener(EventHandler<ActionEvent> listener) {
            listeners.add(listener);
        }

        public void stop() {
            stop = true;
        }

        public int getNumberOfDigits() {
            return numberOfDigits;
        }

        private void countDown() {
            Platform.runLater(() -> {
                toggbuttSTART.setText("3...");
            });
            try {
                Thread.sleep(1000);
                Platform.runLater(() -> {
                    toggbuttSTART.setText("2...");
                });
                Thread.sleep(1000);
                Platform.runLater(() -> {
                    toggbuttSTART.setText("1...");
                });
                Thread.sleep(1000);
                Platform.runLater(() -> {
                    toggbuttSTART.setText("STOP");
                });
            } catch (InterruptedException e) {
                return;
            }
        }

        @Override
        public void run() {
            lastCountdownData.delete(0, lastCountdownData.length());
            numberOfDigits = 0;

            int from = 0;
            int to = 9;
            if (thisType == TYPE.BINARY) {
                to = 1;
            }

            countDown();

            while (shouldContinue() || isStoppedButNotFinishedYet()) {
                int iToDigit = NumberGeneratorModel.generate(from, to);

                media[iToDigit].play();
                ++numberOfDigits;

                if ((numberOfDigits % 10) == 0) {
                    generatedDigits.append(iToDigit + "\n");
                } else {
                    generatedDigits.append(iToDigit + " ");
                }

                updateInterface();

                try {
                    sleepSpecifiedTime(iToDigit);
                } catch (InterruptedException e) {
                    return;
                }

                if (checkExtra.isSelected()) {
                    sleepIfExtraBreak();
                }

                --digitsToFinish;
            }

            finishCountdown();
        }

        private boolean shouldContinue() {
            return !stop && (digitsToFinish > 0);
        }

        private boolean isStoppedButNotFinishedYet() {
            return stop && (digitsToFinish > 0) && (numberOfDigits % 10 != 0);
        }

        private void updateInterface() {
            Platform.runLater(() -> {
                labNumber.setText(String.valueOf(numberOfDigits));
                if (isStoppedButNotFinishedYet()) {
                    toggbuttSTART.setDisable(true);
                    toggbuttSTART.setOpacity(0.5);
                    toggbuttSTART.setText(10 - (numberOfDigits % 10) + " digits to stop...");
                }
            });
        }

        private void sleepSpecifiedTime(int iToDigit) throws NumberFormatException, InterruptedException {
            // we need this to stop the sleep mode if user clicks STOP
            double intervalValue = Double.valueOf(comboboxInterval.getValue().substring(0, 3));
            Thread.sleep(Math.round(1000 * intervalValue));
            media[iToDigit].stop();
        }

        private void sleepIfExtraBreak() {
            int valueEach = Integer.valueOf(comboboxEach.getValue().replaceAll("\\D+", ""));
            int valueExtraInterval = Integer.valueOf(comboboxExtraInterval.getValue().replaceAll("\\D+", ""));
            if ((numberOfDigits % valueEach) == 0) {
                try {
                    Thread.sleep(Math.round(1000 * valueExtraInterval));
                } catch (InterruptedException ex) {

                }
            }

        }

        private void finishCountdown() {
            Platform.runLater(() -> {
                toggbuttSTART.setSelected(false);
                toggbuttSTART.setDisable(false);
                toggbuttSTART.setOpacity(0.94);
                toggbuttSTART.setText("START");

                lastCountdownData.append(generatedDigits.toString());
                for (EventHandler<ActionEvent> l : listeners) {
                    l.handle(new ActionEvent());
                }
            });
        }
    }//----- PlayDigits class
}
