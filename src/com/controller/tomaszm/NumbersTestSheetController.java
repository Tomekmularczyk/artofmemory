package com.controller.tomaszm;

import com.model.tomaszm.CONSTANTS;
import com.model.tomaszm.ChangeTheRoot;
import com.model.tomaszm.Controller;
import com.model.tomaszm.INFO;
import com.model.tomaszm.Main;
import com.model.tomaszm.MyData;
import com.model.tomaszm.NewTableView;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Scanner;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.IndexRange;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import javafx.util.converter.NumberStringConverter;
import org.controlsfx.control.PopOver;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;

public class NumbersTestSheetController extends Controller implements Initializable {

    private NewTableView newTableView;
    private Label labSelectedRow;
    private PopOver popOverAbout;
    private TextArea textAreaSource;
    private BorderPane sourcePane;
    private Stage stageSource;
    private Scene sourceScene; //source window

    @FXML
    private StackPane stackPane;
    @FXML
    private MenuBar menuBar;
    @FXML
    private BorderPane borderPane;
    @FXML
    private Label labEmptyValue, labFilledValue, labTotalValue;
    @FXML
    private MenuItem menuItemSource, menuItemCheck;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        newTableView = new NewTableView();
        labEmptyValue.textProperty().bindBidirectional(newTableView.getNumOfEmptyCells(), new NumberStringConverter());
        labFilledValue.textProperty().bindBidirectional(newTableView.getNumOfFilledCells(), new NumberStringConverter());
        borderPane.setCenter(newTableView);

        menuItemSource.setStyle("-fx-text-fill: rgb(77,128,128)");
        menuItemCheck.setStyle("-fx-text-fill: rgb(230,77,77)");

        initSourceWindow();

        popOverAbout = new PopOver(INFO.getTestSheetInfo());
        popOverAbout.setDetachedTitle("Test sheet info");
        Main.primaryStage.setOnCloseRequest(e -> {  //unfortunatelly we have to do this, because If popOver is visible
            popOverAbout.hide(new Duration(0));	//and user closes application popover causes the problem	
        });
    }

    @Override
    public Pane getMainPane() {
        return stackPane;
    }

    @FXML
    private void menuItemShowMeTheSource(ActionEvent e) {
        popOverAbout.hide();
        if (NewTableView.getEvaluate() == false) {
            Action response = Dialogs.create().owner(Main.primaryStage).title("Confirm").message("You haven't finished yet, are You sure?")
                    .actions(Dialog.Actions.OK, Dialog.Actions.CANCEL).showConfirm();
            if (response == Dialog.Actions.OK) {
                stageSource.show();
            } else {
                // ... user chose CANCEL, or closed the dialog
            }
        } else {
            stageSource.show();
        }
    }

    @FXML
    private void menuItemCloseAction(ActionEvent e) {
        Platform.exit();
    }

    @FXML
    private void menuItemAboutAction(ActionEvent e) {
        popOverAbout.show(menuBar);
    }

    @FXML
    private void menuItemGoToWrittenNumbersGeneratorAction(ActionEvent e) {
        stageSource.close();
        popOverAbout.hide();
        NewTableView.setEvaluate(false);

        ChangeTheRoot change = new ChangeTheRoot();
        if (change.initialize(Main.primaryStage, true, CONSTANTS.WRITTEN_NUMBERS_ROOT.directory, "Written Numbers", true) == false) {
            showErrorDialog();
        } else {
            doFadingTransition(change);
        }
    }

    @FXML
    private void menuItemGoToMainMenuAction(ActionEvent e) {
        popOverAbout.hide();
        stageSource.close();
        NewTableView.setEvaluate(false);
        ChangeTheRoot change = new ChangeTheRoot();

        if (change.initialize(Main.primaryStage, false, CONSTANTS.MAIN_ROOT.directory, "ArtOfMemory", true)) {
            showErrorDialog();
        } else {
            doFadingTransition(change);
        }
    }

    @FXML
    private void menuItemSpokenNumbersAction(ActionEvent e) {
        stageSource.close();
        popOverAbout.hide();
        NewTableView.setEvaluate(false);

        ChangeTheRoot changeToSpoken = new ChangeTheRoot();
        if (changeToSpoken.initialize(Main.primaryStage, false, CONSTANTS.SPOKEN_NUMBERS_ROOT.directory, "Spoken Numbers", true) == false) {
            showErrorDialog();
        } else {
            doFadingTransition(changeToSpoken);
        }
    }

    private void showErrorDialog() {
        Dialogs.create().title("Error").masthead(null).message("Couldn't load the components!").showError();
    }

    private void doFadingTransition(ChangeTheRoot changeToSpoken) {
        changeToSpoken.doFadingTransition(stackPane, CONSTANTS.fadeOutMillis.value, CONSTANTS.fadeInMillis.value, true);
    }

    @FXML
    private void menuItemCheckAction(ActionEvent e) {
        popOverAbout.hide();
        compareAndEvaluate(newTableView);
    }

    public void initData(int nrOfColumns, int nrOfRows, String source) {
        textAreaSource.setText(source);

        stackPane.setPrefSize((nrOfColumns * 25) + 30, (nrOfRows * 25) + 70);
        source = source.replaceAll("\\D+", "");

        newTableView.addRowsAndColumn(nrOfColumns + 1, nrOfRows);

        //we are seting coresponding valid data to each of table cell so later we can match them
        ObservableList<MyData> data = newTableView.getData();
        int x = 0; //index of string
        for (int i = 0; i < data.size(); i++) {
            ArrayList<Integer> corespondingValues = new ArrayList<>();
            ObservableList<ObjectProperty<Integer>> rowData = data.get(i).returnCellsData();
            for (int j = 1; j < rowData.size(); j++, x++) { //1 because first its row name
                corespondingValues.add(Integer.valueOf(String.valueOf(source.charAt(x))));  //rows from the source has to be equal lenght
            }
            data.get(i).setCorespondingValues(corespondingValues);
        }

        //showCorespondingCellValues();
        labTotalValue.setText(String.valueOf(nrOfColumns * nrOfRows));
    }

    private void initSourceWindow() {
        textAreaSource = new TextArea();
        textAreaSource.setFont(new Font(14));
        textAreaSource.setEditable(false);
        textAreaSource.selectionProperty().addListener(new ChangeListener<IndexRange>() {
            @Override
            public void changed(ObservableValue<? extends IndexRange> observable, IndexRange oldValue, IndexRange newValue) {
                updateSelectedRow(newValue.getStart());
            }
        });
        labSelectedRow = new Label("Selected row: ");
        labSelectedRow.setFont(new Font("Cambria", 11.0));
        labSelectedRow.setOpacity(0.6);
        sourcePane = new BorderPane(textAreaSource);
        sourcePane.setBottom(labSelectedRow);
        sourcePane.setPrefSize(320, 150);

        stageSource = new Stage();
        stageSource.getIcons().add(new Image(CONSTANTS.APPLICATION_ICON.directory));
        sourceScene = new Scene(sourcePane);
        stageSource.setScene(sourceScene);
        stageSource.initOwner(Main.primaryStage);
        stageSource.initModality(Modality.NONE);
        stageSource.initStyle(StageStyle.UTILITY);
        stageSource.setTitle("Source digits");
    }

    private void compareAndEvaluate(NewTableView newTableView) {
        NewTableView.setEvaluate(true);
        newTableView.getColumns().get(0).setVisible(false);
        newTableView.getColumns().get(0).setVisible(true);

        //quick info about results
        ObservableList<MyData> data = newTableView.getData();
        int numOfValid = 0;
        int numOfInvalid = 0;
        for (int i = 0; i < data.size(); i++) {           //rows(Y)
            ObservableList<ObjectProperty<Integer>> rowData = data.get(i).returnCellsData();
            for (int j = 1; j < rowData.size(); j++) {    //columns(X)
                Integer digit = rowData.get(j).get();
                Integer correspondingDigit = data.get(i).getCorespondingValue(j - 1);
                
                if (digit != null && digit.equals(correspondingDigit)) {
                    ++numOfValid;
                } else {
                    ++numOfInvalid;
                }
            }
        }
        showResults(numOfValid, numOfInvalid);
    }

    private void showResults(int numOfValid, int numOfInvalid) {
        Dialogs.create()
                .title("Results")
                .masthead("Correct: " + numOfValid + ", Invalid: " + numOfInvalid)
                .message("You can now try to correct the sheet, values will be automatically veryfied.")
                .showInformation();
    }

    private void updateSelectedRow(int position) {
        labSelectedRow.setText("Selected row: " + getLinePosition(textAreaSource.getText(), position));
    }

    private int getLinePosition(String text, int position) {
        try (Scanner scanner = new Scanner(text)) {
            int pos = 0;
            int line = 0;
            while ((scanner.hasNextLine() == true) && (pos < position)) {
                String s = scanner.nextLine();
                pos += s.length();
                if (s.isEmpty() == false) {
                    ++line;
                }
            }

            return line;
        }
    }

    @SuppressWarnings("unused")
    private void showCorespondingCellValues() { //for a programmer to see earlier generated digits
        ObservableList<MyData> data = newTableView.getData();
        for (int i = 0; i < data.size(); i++) {
            System.out.println();
            ObservableList<ObjectProperty<Integer>> rowData = data.get(i).returnCellsData();
            for (int j = 1; j < rowData.size(); j++) {
                System.out.print(data.get(i).getCorespondingValue(j - 1));
            }
        }
    }
}
