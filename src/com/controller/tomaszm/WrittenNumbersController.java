package com.controller.tomaszm;

import com.model.tomaszm.CONSTANTS;
import com.model.tomaszm.ChangeTheRoot;
import com.model.tomaszm.Controller;
import com.model.tomaszm.INFO;
import com.model.tomaszm.Main;
import com.model.tomaszm.NumberGeneratorModel;
import com.model.tomaszm.SettingsPane;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Scanner;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.IndexRange;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.util.Duration;
import javafx.util.converter.NumberStringConverter;
import org.controlsfx.control.PopOver;
import org.controlsfx.control.PopOver.ArrowLocation;
import org.controlsfx.dialog.Dialogs;

public class WrittenNumbersController extends Controller implements Initializable {

    private int digitsInRow = 0;
    private int numOfRows = 0; //we need value that is not bindable
    private int numOfDraws = 0;

    //----- options
    private IntegerProperty numberOfRows = new SimpleIntegerProperty(10);
    private IntegerProperty numberOfDraws = new SimpleIntegerProperty(10);
    private StringProperty suffix = new SimpleStringProperty("");
    private StringProperty preffix = new SimpleStringProperty("");
    private BooleanProperty spaceTheDraws = new SimpleBooleanProperty(true);
    private BooleanProperty spaceTheLines = new SimpleBooleanProperty(false);
    private BooleanProperty checkOnlyOnce = new SimpleBooleanProperty(false);
    private Font font = new Font("Times New Roman", 16);

    private BorderPane stoperPane;
    private PopOver popOverTimer;
    private PopOver popOverSettings;
    private SettingsPane settingsPane;
    private StackPane countdownPane;
    private PopOver popOverAbout;

    @FXML
    private MenuItem menuItemTryMe, menuItemClose;
    @FXML
    private TextArea textArea;
    @FXML
    private Button buttGenerate;
    @FXML
    private TextField fieldMinV;
    @FXML
    private TextField fieldMaxV;
    @FXML
    private BorderPane borderPane;
    @FXML
    private MenuBar menuBar;
    @FXML
    private Label labDigits, labDraws, labDigitsInRow, labSelectedRow;

    private StoperController stoperController;
    private CountdownController countdownController;
    private boolean isCounting = false;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        menuItemTryMe.setStyle("-fx-text-fill: rgb(230,77,77)");
        buttGenerate.setTooltip(INFO.tooltipButtGenerate);
        INFO.tooltipDigitsInfo.setOpacity(0.9);
        INFO.tooltipDrawsInfo.setOpacity(0.9);
        labDigits.setTooltip(INFO.tooltipDigitsInfo);
        labDraws.setTooltip(INFO.tooltipDrawsInfo);

        initStoper();
        initCountdownPane();

        textArea.setFont(font);
        textArea.selectionProperty().addListener(new ChangeListener<IndexRange>() {
            @Override
            public void changed(ObservableValue<? extends IndexRange> observable, IndexRange oldValue, IndexRange newValue) {
                updateSelectedRow(newValue.getStart());
            }
        });

        initSettings();
        initPopOvers();
    }

    private void initPopOvers() {
        popOverTimer = new PopOver(stoperPane);
        popOverTimer.setArrowLocation(ArrowLocation.RIGHT_TOP);
        popOverTimer.setDetachedTitle("Timer");
        popOverSettings = new PopOver(settingsPane);
        popOverSettings.setArrowLocation(ArrowLocation.TOP_CENTER);
        popOverSettings.setDetachedTitle("Settings");
        popOverSettings.setArrowSize(20);
        popOverAbout = new PopOver(INFO.getWrittenNumbersInfo());
        popOverAbout.setDetachedTitle("Written numbers info");

        Main.primaryStage.setOnCloseRequest(e -> {
            popOverSettings.hide(new Duration(0));  //unfortunatelly we have to do this, because If popOver is visible
            popOverTimer.hide(new Duration(0));      //and user closes application popover causes the problem
            popOverAbout.hide(new Duration(0));
        });
    }

    private void initCountdownPane() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(CONSTANTS.COUNTDOWN_ROOT.directory));
            countdownPane = loader.load();
            countdownController = loader.getController();
            countdownController.getButtSTOP().setOnAction(e -> {
                popOverTimer.setContentNode(stoperPane);
                countdownController.STOP();
                isCounting = false;
            });
            countdownController.setOnAction(e -> {   //when countdown comes to end
                popOverTimer.setContentNode(stoperPane);
                if (stoperController.getCheckTestWhenFinished().isSelected() == true) {
                    menuItemTryMe(e);
                }
            });
        } catch (IOException e) {
            showErrorDialogWithException(e);
            e.printStackTrace();
        }
    }

    private void showErrorDialogWithException(IOException e) {
        Dialogs.create().title("Exception").masthead(null).message("Couldnt load the fxml!").showException(e);
    }

    private void initStoper() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(CONSTANTS.STOPER_ROOT.directory));
            stoperPane = loader.load();
            stoperController = loader.getController();
            stoperController.getButtSTART().setOnAction(e -> {
                popOverTimer.setContentNode(countdownPane);
                if (stoperController.getCheckGenerateNewNumbers().isSelected()) {
                    buttGenerateAction(e);
                }
                countdownController.initCountdownController(stoperController.getComboBoxHours().getValue(),
                        stoperController.getComboBoxMinutes().getValue(),
                        stoperController.getComboBoxSeconds().getValue());
                countdownController.START();
                isCounting = true;
            });
        } catch (IOException e) {
            showErrorDialogWithException(e);
            e.printStackTrace();
        }
    }

    @Override
    public Pane getMainPane() {
        return borderPane;
    }

    private void stopCounting() {
        if (isCounting == true) {
            countdownController.STOP();
        }
    }

    /**
     * *************METHODS****************
     */
    private void updateSelectedRow(int position) {
        labSelectedRow.setText("Selected row: " + getLinePosition(textArea.getText(), position));
    }

    private int getLinePosition(String text, int position) {
        Scanner scanner = new Scanner(text);
        int pos = 0;
        int line = 0;
        while ((scanner.hasNextLine() == true) && (pos < position)) {
            String s = scanner.nextLine();
            pos += s.length();
            if (s.isEmpty() == false) {
                ++line;
            }
        }
        scanner.close();
        return line;
    }

    private void initSettings() {
        settingsPane = new SettingsPane();

        settingsPane.buttDone.setOnAction(event -> {
            popOverSettings.hide();
        });

        settingsPane.pickerTextColor.setOnAction(event -> {
            String textColor = settingsPane.pickerTextColor.getValue().toString();
            textColor = textColor.substring(2);
            textArea.setStyle("-fx-text-fill: #" + textColor);
        });

        settingsPane.pickerBackgroundColor.setOnAction(event -> {
            String backgroundColor = settingsPane.pickerBackgroundColor.getValue().toString();
            backgroundColor = backgroundColor.substring(2);
            Node node = textArea.lookup(".content");
            node.setStyle("-fx-background-color: #" + backgroundColor);
        });

        settingsPane.hyperSetFont.setOnAction(event -> {
            Optional<Font> response = Dialogs.create().owner(settingsPane).masthead("Choose what you like").showFontSelector(font);
            if (response.isPresent() == true) {
                font = response.get();
                textArea.setFont(font);
            }
        });

        initBindings();
    }

    private void initBindings() {
        Bindings.bindBidirectional(settingsPane.checkSpaceTheDraws.selectedProperty(), spaceTheDraws);
        Bindings.bindBidirectional(settingsPane.checkSpaceTheLines.selectedProperty(), spaceTheLines);
        Bindings.bindBidirectional(settingsPane.checkOnlyOnce.selectedProperty(), checkOnlyOnce);
        Bindings.bindBidirectional(settingsPane.textfPreffix.textProperty(), preffix);
        Bindings.bindBidirectional(settingsPane.textfSuffix.textProperty(), suffix);
        settingsPane.labRowsValue.textProperty().bindBidirectional(numberOfRows, new NumberStringConverter());
        settingsPane.labDrawsValue.textProperty().bindBidirectional(numberOfDraws, new NumberStringConverter());
    }

    private void updateInfo() {
        String sDigits = "Digits: " + (digitsInRow * numOfRows);
        String sDigitsInLine = "Digits in row: " + digitsInRow;
        String sDraws = "Numbers(draws): " + (numOfDraws * numOfRows);

        labDigits.setText(sDigits);
        labDraws.setText(sDraws);
        labDigitsInRow.setText(sDigitsInLine);
    }

    @FXML
    private void buttGenerateAction(ActionEvent e) {
        NumberGeneratorModel.setPreffixSuffix(preffix.get(), suffix.get());
        NumberGeneratorModel.setSpacing(true, spaceTheLines.get(), spaceTheDraws.get());

        boolean eachNumberOnlyOnce = checkOnlyOnce.get();
        NumberGeneratorModel.generateNumbers(fieldMinV, fieldMaxV, textArea, numberOfRows.get(), numberOfDraws.get(), eachNumberOnlyOnce);

        //updating bottom info
        NumberGeneratorModel.FillArea.setOnFinishAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Scanner scanner = new Scanner(textArea.getText());
                if (scanner.hasNext()) {
                    String str = scanner.nextLine();
                    str = str.replaceAll("\\D+", "");
                    digitsInRow = str.length();
                } else {
                    digitsInRow = 0;
                }

                if (numberOfDraws.get() < 1) {
                    numOfRows = 0;
                } else {
                    Scanner scanner2 = new Scanner(textArea.getText());
                    int line = 1;
                    while (scanner.hasNextLine()) {
                        scanner.nextLine();
                        ++line;
                    }
                    numOfRows = line;
                    scanner2.close();
                }
                numOfDraws = numberOfDraws.get();
                scanner.close();
                updateInfo();
            }
        });
    }

    @FXML
    private void menuItemGoToMainMenuAction(ActionEvent e) {
        hidePopOvers();
        stopCounting();

        ChangeTheRoot change = new ChangeTheRoot();
        if (!change.initialize(Main.primaryStage, false, CONSTANTS.MAIN_ROOT.directory, "ArtOfMemory", true)) {
            showErrorDialog();
        } else {
            change.doFadingTransition(borderPane, CONSTANTS.fadeOutMillis.value, CONSTANTS.fadeInMillis.value, true);
        }
    }

    private void hidePopOvers() {
        popOverTimer.hide();
        popOverSettings.hide();
        popOverAbout.hide();
    }

    @FXML
    private void menuItemTryMe(ActionEvent e) {
        hidePopOvers();
        stopCounting();

        if ((numOfRows > 0) && (numOfDraws > 0)) {
            ChangeTheRoot change = new ChangeTheRoot();
            if (!change.initialize(Main.primaryStage, true, CONSTANTS.NUMBERS_TEST_SHEET_ROOT.directory, "Test sheet", true)) {
                showErrorDialog();
            } else {
                //getting how many rows and columns we need
                int number_of_rows = getLinePosition(textArea.getText(), textArea.getText().length()); // in case OnlyOnce method was used
                ((NumbersTestSheetController) change.getController()).initData(digitsInRow, number_of_rows, textArea.getText());
                change.doFadingTransition(borderPane, CONSTANTS.fadeOutMillis.value, CONSTANTS.fadeInMillis.value);
            }
        } else {
            Dialogs.create().title("Exception").masthead(null).message("First You need to generate numbers!").showInformation();
        }
    }

    private void showErrorDialog() {
        Dialogs.create().title("Error").masthead(null).message("Can't load the components!").showError();
    }

    @FXML
    private void menuItemSaveToFileAction(ActionEvent e) {
        hidePopOvers();

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save File");
        fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Text Files", "*.txt"));
        File selectedFile = fileChooser.showSaveDialog(Main.primaryStage);
        if (selectedFile != null) {
            try {
                PrintWriter printWriter = new PrintWriter(selectedFile);
                Scanner scanner = new Scanner(textArea.getText());

                while (scanner.hasNext()) {
                    printWriter.println(scanner.nextLine() + "\n");
                }

                printWriter.close();
                scanner.close();
            } catch (FileNotFoundException e1) {
                Dialogs.create().title("Error").masthead(null).message("Couldn't save the file!").showError();
            }

        }
    }

    @FXML
    private void menuItemClearAction(ActionEvent e) {
        stopCounting(); //in case its open thread
        textArea.clear();
        digitsInRow = 0;
        numOfRows = 0;
        updateInfo();
    }

    @FXML
    private void menuItemSettingsAction(ActionEvent e) {
        popOverSettings.show(menuBar);
    }

    @FXML
    private void menuItemSetTheTimerAction(ActionEvent e) {
        popOverTimer.show(menuBar);
    }

    @FXML
    private void menuItemCloseAction(ActionEvent e) {
        Platform.exit();
    }

    @FXML
    private void menuItemAboutAction(ActionEvent e) {
        popOverAbout.show(menuBar);
    }
}
