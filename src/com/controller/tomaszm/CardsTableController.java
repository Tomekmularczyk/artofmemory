package com.controller.tomaszm;

import com.model.tomaszm.CONSTANTS;
import com.model.tomaszm.DeckOfCards;
import com.model.tomaszm.DeckOfCards.CardPane;
import com.model.tomaszm.DeckOfCards.CardSuit;
import com.model.tomaszm.DeckOfCards.CardValue;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;

public class CardsTableController implements Initializable {

    private DeckOfCards deckOfMixedCards = new DeckOfCards();
    private final DeckOfCards deckOfOrderedCards = new DeckOfCards();

    @FXML
    private TilePane tilePane;
    @FXML
    private Pane paneInfo;
    @FXML
    private ScrollPane scrollMixedDeck, scrollNewDeck;
    @FXML
    private BorderPane borderPane, borderTopTable;
    @FXML
    private AnchorPane anchorBottomDeck;
    @FXML
    private SplitPane splitPane;
    @FXML
    private Label labNrOfCorrect, labNrOfWrong;

    public void newDeck() {
        deckOfMixedCards.newUnshuffledDeck();
    }

    public void shuffle() {
        deckOfMixedCards.mixDeck();
    }

    public void test() {
        deckOfMixedCards.removeCards();
        deckOfMixedCards.makeCardPanesListening();
        deckOfMixedCards.makeEnterExitEffect();

        layoutOrderedCards(anchorBottomDeck, deckOfOrderedCards);
        if (borderTopTable.getRight() != null) {
            borderTopTable.setRight(null);
        }

        deckOfOrderedCards.makeCardPanesListening();
        deckOfOrderedCards.makeCardsDraggable();
    }

    public void finishTest() {
        anchorBottomDeck.getChildren().clear();
        deckOfMixedCards.removeLabels();

        deckOfMixedCards.newUnshuffledDeck();
        deckOfOrderedCards.newUnshuffledDeck();

        if (borderTopTable.getRight() != null) {
            borderTopTable.setRight(null);
        }
    }

    public void setSplitPaneDevider(double position) {
        splitPane.setDividerPositions(position, position);
    }

    public String getCardsLayoutOrder() {
        return deckOfMixedCards.getCardsLayoutOrder();
    }

    public int checkDeck() {
        int count = 0;
        for (CardValue cardValue : CardValue.values()) {
            for (CardSuit cardSuit : CardSuit.values()) {
                if (deckOfMixedCards.isRight(cardValue, cardSuit) == true) {
                    ++count;
                }
            }
        }
        deckOfMixedCards.showNameLabels();

        labNrOfCorrect.setText(String.valueOf(count));
        labNrOfWrong.setText(String.valueOf(52 - count));

        if (borderTopTable.getRight() == null) {
            borderTopTable.setRight(paneInfo);
        }
        return count;
    }

    /**
     * *********************************************************
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        scrollMixedDeck.setFitToHeight(true);
        scrollMixedDeck.setFitToWidth(true);	//--- we need to do this to set content of scrollpane resizable with it
        scrollNewDeck.setFitToHeight(true);
        scrollNewDeck.setFitToWidth(true);
        borderTopTable.setRight(null); //we delete Info Pane at start

        tilePane.setBackground(new Background(new BackgroundImage(new Image(CONSTANTS.CARDS_TABLE_BACKGROUND.directory),
                BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT,
                BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT)));
        anchorBottomDeck.setBackground(new Background(new BackgroundImage(new Image(CONSTANTS.CARDS_TABLE_BACKGROUND.directory),
                BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT,
                BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT)));
        paneInfo.setBackground(new Background(new BackgroundImage(new Image(CONSTANTS.CARDS_INFO_BACKGROUND.directory),
                BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT,
                BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT)));
        layoutMixedCards(tilePane, deckOfMixedCards);

        deckOfOrderedCards.setStyle("-fx-background-color: rgba(153,179,255,0.6);");
    }

    private void layoutMixedCards(TilePane flowPane, DeckOfCards deck) {
        for (CardValue cardValue : CardValue.values()) {
            for (CardSuit cardSuit : CardSuit.values()) {
                flowPane.getChildren().add(deck.getCard(cardValue, cardSuit));
            }
        }

    }

    private void layoutOrderedCards(AnchorPane pane, DeckOfCards deck) {
        int offsetX = 15, offsetY = 15, spaces = 30;

        for (int i = 0; i < CardValue.values().length; i++) {
            for (int j = 0; j < CardSuit.values().length; j++) {
                CardPane card = deck.getCard(i, j);
                card.setLayoutX(offsetX + (((i * 4) + j) * spaces));
                card.setLayoutY(offsetY);
                card.setRotate(-25);
                pane.getChildren().add(card);
            }
        }

    }

}
