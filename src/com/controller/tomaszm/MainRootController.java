package com.controller.tomaszm;

import com.model.tomaszm.CONSTANTS;
import com.model.tomaszm.ChangeTheRoot;
import com.model.tomaszm.Controller;
import com.model.tomaszm.INFO;
import com.model.tomaszm.Main;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.Callback;
import javafx.util.Duration;
import org.controlsfx.control.PopOver;
import org.controlsfx.control.PopOver.ArrowLocation;
import org.controlsfx.dialog.Dialogs;

public class MainRootController extends Controller implements Initializable {

    private PopOver popOverGeneralInfo;

    @FXML
    private StackPane stackPane;
    @FXML
    private ListView<ListOption> listView;

    @FXML
    private void menuItemAboutAction(ActionEvent e) {
        popOverGeneralInfo.show(stackPane);
    }

    private ObservableList<ListOption> list;

    private enum ListOption {
        WRITTEN_NUMBERS("Written Numbers"), SPOKEN_NUMBERS("Spoken Numbers"), CARDS("Cards");
        private String title;

        private ListOption(String title) {
            this.title = title;
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ListOption[] l = ListOption.values();

        popOverGeneralInfo = new PopOver(INFO.getGeneralInfoPane());
        popOverGeneralInfo.setArrowLocation(ArrowLocation.RIGHT_TOP);
        popOverGeneralInfo.setDetachedTitle("General Info");
        Main.primaryStage.setOnCloseRequest(e -> {  //unfortunatelly we have to do this, because If popOver is visible
            popOverGeneralInfo.hide(new Duration(0));      //and user closes application popover causes the problem										
        });

        //making a list and setting a background
        list = FXCollections.observableArrayList(l);

        listView.setId("mainList");
        listView.setBackground(new Background(new BackgroundImage(new Image(CONSTANTS.MAIN_ROOT_BACKGROUND.directory),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));
        listView.setItems(list);
        listView.setCellFactory(new Callback<ListView<ListOption>, ListCell<ListOption>>() {
            @Override
            public ListCell<ListOption> call(ListView<ListOption> list) {
                return new MouseClickListCell();
            }

        });
        listView.getSelectionModel().clearSelection();
    }

    @Override
    public Pane getMainPane() {
        return stackPane;
    }

    /**
     * ********************** EVENTS ************************
     */
    private void fireUpTheFeature(ListOption selectedOption) {
        ChangeTheRoot changeTheRoot = new ChangeTheRoot();
        popOverGeneralInfo.hide();
        
        boolean isInitialized = false;
        switch (selectedOption) {
            case WRITTEN_NUMBERS:
                isInitialized = changeTheRoot.initialize(Main.primaryStage, true, CONSTANTS.WRITTEN_NUMBERS_ROOT.directory, ListOption.WRITTEN_NUMBERS.title, true);
                break;
            case SPOKEN_NUMBERS:
                isInitialized = changeTheRoot.initialize(Main.primaryStage, false, CONSTANTS.SPOKEN_NUMBERS_ROOT.directory, ListOption.SPOKEN_NUMBERS.title, true);
                break;
            case CARDS:
                isInitialized = changeTheRoot.initialize(Main.primaryStage, true, CONSTANTS.CARDS_ROOT.directory, ListOption.CARDS.title, true);
                break;
            default:
                break;
        }

        if(isInitialized){
            doFadingTransition(changeTheRoot);
        } else {
            showErrorDialog();
        }
    }

    private void doFadingTransition(ChangeTheRoot changeToWritten) {
        changeToWritten.doFadingTransition(stackPane, CONSTANTS.fadeOutMillis.value, CONSTANTS.fadeInMillis.value, true);
    }

    private void showErrorDialog() {
        Dialogs.create().title("Error").masthead(null).message("Couldn't load the components!").showError();
    }

    public class MouseClickListCell extends ListCell<ListOption> {

        @Override
        protected void updateItem(ListOption item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setOnMouseClicked(null);
            } else {
                setText(item.title);
                setOnMouseClicked((MouseEvent event) -> {
                    if (event.getClickCount() > 1) {
                        fireUpTheFeature(getItem());
                    }
                });
            }
        }
    }

    @FXML
    private void menuItemCloseAction(ActionEvent e) {
        Platform.exit();
    }
}
