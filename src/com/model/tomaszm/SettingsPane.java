package com.model.tomaszm;

import java.text.NumberFormat;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.effect.MotionBlur;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.converter.NumberStringConverter;

public class SettingsPane extends BorderPane {

    private HBox hBox;
    private TabPane tabPane;
    private Tab tabGenerator, tabDraws, tabLooks;
    private AnchorPane paneTabGenerator, paneTabDraws, paneTabLooks;
    private Button buttPlusRow, buttPlusDraw, buttMinusRow, buttMinusDraw;
    private Slider sliderRows, sliderDraws;
    private Label labNumOfDraws, labNumOfRows,
            labTextColor, labBackgroundColor,
            labSuffix, labPreffix;
    public CheckBox checkAddZeros, checkOnlyOnce;
    public Hyperlink hyperSetFont;
    public ColorPicker pickerTextColor, pickerBackgroundColor;
    public Button buttDone;
    public Label labDrawsValue, labRowsValue;
    public CheckBox checkSpaceTheDraws, checkSpaceTheLines;
    public TextField textfSuffix, textfPreffix;

    public SettingsPane() {
        super();
        this.setPrefWidth(217);
        initComponents();
    }

    private void initComponents() {
        int layoutX = 5, layoutY = 10;

        MotionBlur motionBlur = new MotionBlur();
        motionBlur.setRadius(3);
        motionBlur.setAngle(-15.0);

        initPanes();
        initButtons(layoutX, layoutY);
        initTabs();
        initLabels(layoutX, layoutY);
        initHyperLinks(layoutX, layoutY);
        initTextFields(layoutX, layoutY);
        initSliders(layoutX, layoutY);
        initPickers(layoutX, layoutY);
        initCheckboxes(layoutX, layoutY, motionBlur);
        initActionListeners();

        initBindings();

        // ---- adding controls to panes
        HBox hBoxDraws = new HBox();
        hBoxDraws.setLayoutX(layoutX);
        hBoxDraws.setLayoutY(layoutY);
        hBoxDraws.getChildren().addAll(labNumOfDraws, labDrawsValue);
        HBox hBoxRows = new HBox();
        hBoxRows.setLayoutX(layoutX);
        hBoxRows.setLayoutY(layoutY + 65);
        hBoxRows.getChildren().addAll(labNumOfRows, labRowsValue);
        HBox hBoxSuffix = new HBox();
        hBoxSuffix.setSpacing(5);
        hBoxSuffix.setLayoutX(layoutX);
        hBoxSuffix.setLayoutY(layoutY + 4);
        hBoxSuffix.getChildren().addAll(labSuffix, textfSuffix);
        HBox hBoxPrefix = new HBox();
        hBoxPrefix.setSpacing(5);
        hBoxPrefix.setLayoutX(layoutX);
        hBoxPrefix.setLayoutY(layoutY + 44);
        hBoxPrefix.getChildren().addAll(labPreffix, textfPreffix);
        HBox hBoxTextColor = new HBox();
        hBoxTextColor.setSpacing(2);
        hBoxTextColor.setLayoutX(layoutX);
        hBoxTextColor.setLayoutY(layoutY + 3);
        hBoxTextColor.getChildren().addAll(labTextColor, pickerTextColor);
        HBox hBoxBackgroundColor = new HBox();
        hBoxBackgroundColor.setSpacing(2);
        hBoxBackgroundColor.setLayoutX(layoutX);
        hBoxBackgroundColor.setLayoutY(layoutY + 38);
        hBoxBackgroundColor.getChildren().addAll(labBackgroundColor, pickerBackgroundColor);
        hBox.getChildren().add(buttDone);
        tabPane.getTabs().addAll(tabGenerator, tabDraws, tabLooks);
        tabLooks.setContent(paneTabLooks);
        tabDraws.setContent(paneTabDraws);
        tabGenerator.setContent(paneTabGenerator);

        paneTabGenerator.getChildren().addAll(hBoxDraws,
                sliderDraws, hBoxRows, sliderRows,
                buttPlusDraw, buttMinusDraw, buttPlusRow, buttMinusRow, checkOnlyOnce);
        paneTabDraws.getChildren().addAll(hBoxSuffix, hBoxPrefix, checkAddZeros);
        paneTabLooks.getChildren().addAll(hBoxTextColor, hBoxBackgroundColor, checkSpaceTheDraws,
                checkSpaceTheLines, hyperSetFont);
    }

    private void initPanes() {
        // ----- panes
        tabPane = new TabPane();
        hBox = new HBox();
        hBox.setAlignment(Pos.BOTTOM_RIGHT);
        hBox.setSpacing(1);
        paneTabGenerator = new AnchorPane();
        paneTabDraws = new AnchorPane();
        paneTabLooks = new AnchorPane();
        super.setCenter(tabPane);
        super.setBottom(hBox);
    }

    private void initBindings() {
        // ---- binding
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(0);
        labDrawsValue.textProperty().bindBidirectional(sliderDraws.valueProperty(), new NumberStringConverter(nf));
        labRowsValue.textProperty().bindBidirectional(sliderRows.valueProperty(), new NumberStringConverter(nf));
        Bindings.bindBidirectional(checkOnlyOnce.selectedProperty(), sliderRows.disableProperty());
        Bindings.bindBidirectional(checkOnlyOnce.selectedProperty(), buttPlusRow.disableProperty());
        Bindings.bindBidirectional(checkOnlyOnce.selectedProperty(), buttMinusRow.disableProperty());
        Bindings.bindBidirectional(checkOnlyOnce.selectedProperty(), labRowsValue.disableProperty());
        Bindings.bindBidirectional(checkOnlyOnce.selectedProperty(), labNumOfRows.disableProperty());
    }

    private void initActionListeners() {
        // ---- action listeners
        buttPlusDraw.setOnAction(this::buttPlusDrawAction);
        buttMinusDraw.setOnAction(this::buttMinusDrawAction);
        buttPlusRow.setOnAction(this::buttPlusRowAction);
        buttMinusRow.setOnAction(this::buttMinusRowAction);
    }

    private void initCheckboxes(int layoutX, int layoutY, MotionBlur motionBlur) {
        checkSpaceTheDraws = new CheckBox("Space the draws");
        checkSpaceTheDraws.setSelected(true);
        checkSpaceTheDraws.setLayoutX(layoutX);
        checkSpaceTheDraws.setLayoutY(layoutY + 75);
        checkSpaceTheLines = new CheckBox("Space the lines");
        checkSpaceTheLines.setLayoutX(layoutX);
        checkSpaceTheLines.setLayoutY(layoutY + 100);
        checkAddZeros = new CheckBox("AddZeros");
        checkAddZeros.setEffect(motionBlur);
        checkAddZeros.setSelected(true);
        checkAddZeros.setDisable(true);
        checkAddZeros.setLayoutX(layoutX);
        checkAddZeros.setLayoutY(layoutY + 100);
        checkOnlyOnce = new CheckBox("Each number only once");
        checkOnlyOnce.setTooltip(INFO.tooltipEachNumOnlyOnce);
        checkOnlyOnce.setLayoutX(layoutX);
        checkOnlyOnce.setLayoutY(layoutY + 130);

        checkOnlyOnce.setOnAction(e -> {
            if (checkOnlyOnce.isSelected()) {
                labNumOfRows.setDisable(true);
                sliderRows.setDisable(true);
                buttMinusRow.setDisable(true);
                buttPlusRow.setDisable(true);
            } else {
                labNumOfRows.setDisable(false);
                sliderRows.setDisable(false);
                buttMinusRow.setDisable(false);
                buttPlusRow.setDisable(false);
            }
        });
    }

    private void initPickers(int layoutX, int layoutY) {
        pickerTextColor = new ColorPicker(Color.BLACK);
        pickerTextColor.setLayoutX(layoutX + 60);
        pickerTextColor.setLayoutY(layoutY);
        pickerBackgroundColor = new ColorPicker(Color.WHITE);
        pickerBackgroundColor.setLayoutX(layoutX + 101);
        pickerBackgroundColor.setLayoutY(layoutY + 35);
    }

    private void initSliders(int layoutX, int layoutY) {
        sliderDraws = new Slider(0, 100, 10);
        sliderDraws.setTooltip(INFO.tooltipSliderDraws);
        sliderDraws.setShowTickLabels(true);
        sliderDraws.setShowTickMarks(true);
        sliderDraws.setMajorTickUnit(10);
        sliderDraws.setMinorTickCount(1);
        sliderDraws.setBlockIncrement(1);
        sliderDraws.setPrefSize(150, 15);
        sliderDraws.setLayoutX(layoutX);
        sliderDraws.setLayoutY(layoutY + 20);
        sliderRows = new Slider(0, 250, 10);
        sliderRows.setShowTickLabels(true);
        sliderRows.setShowTickMarks(true);
        sliderRows.setMajorTickUnit(50);
        sliderRows.setMinorTickCount(1);
        sliderRows.setBlockIncrement(1);
        sliderRows.setPrefSize(150, 15);
        sliderRows.setLayoutX(layoutX);
        sliderRows.setLayoutY(layoutY + 85);
    }

    private void initTextFields(int layoutX, int layoutY) {
        textfSuffix = new TextField("");
        textfSuffix.setTooltip(INFO.tooltipSuffix);
        textfSuffix.setPrefWidth(50);
        textfSuffix.setLayoutX(layoutX + 60);
        textfSuffix.setLayoutY(layoutY);
        textfPreffix = new TextField("");
        textfPreffix.setTooltip(INFO.tooltipPreffix);
        textfPreffix.setPrefWidth(50);
        textfPreffix.setLayoutX(layoutX);
        textfPreffix.setLayoutY(layoutY + 40);
    }

    private void initHyperLinks(int layoutX, int layoutY) {
        hyperSetFont = new Hyperlink("Set font...");
        hyperSetFont.setFont(new Font(14));
        hyperSetFont.setLayoutX(layoutX - 5);
        hyperSetFont.setLayoutY(layoutY + 130);
    }

    private void initLabels(int layoutX, int layoutY) {
        labNumOfDraws = new Label("Draws in line:");
        labNumOfRows = new Label("Number of rows:");
        labDrawsValue = new Label("10");
        labDrawsValue.setTextFill(Color.RED);
        labDrawsValue.setLayoutX(layoutX + 75);
        labDrawsValue.setLayoutY(layoutY);
        labRowsValue = new Label("100");
        labRowsValue.setTextFill(Color.RED);
        labRowsValue.setLayoutX(layoutX + 93);
        labRowsValue.setLayoutY(layoutY + 65);
        labTextColor = new Label("Text color:");
        labTextColor.setLayoutX(layoutX);
        labTextColor.setLayoutY(layoutY + 3);
        labBackgroundColor = new Label("Background color:");
        labBackgroundColor.setLayoutX(layoutX);
        labBackgroundColor.setLayoutY(layoutY + 38);
        labSuffix = new Label("Add suffix:");
        labSuffix.setLayoutX(layoutX);
        labSuffix.setLayoutY(layoutY + 4);
        labPreffix = new Label("Add preffix:");
        labPreffix.setLayoutX(layoutX + 57);
        labPreffix.setLayoutY(layoutY + 44);
    }

    private void initTabs() {
        tabGenerator = new Tab("Generator");
        tabGenerator.setClosable(false);
        tabDraws = new Tab("Draws");
        tabDraws.setClosable(false);
        tabLooks = new Tab(" Looks ");
        tabLooks.setClosable(false);
    }

    private void initButtons(int layoutX, int layoutY) {
        // ---- controls
        buttDone = new Button("Done");
        buttDone.setPrefWidth(70);
        buttDone.setDefaultButton(true);
        buttPlusDraw = new Button("+");
        buttPlusDraw.setTextFill(Color.RED);
        buttPlusDraw.setPrefWidth(25);
        buttPlusDraw.setLayoutX(layoutX + 155);
        buttPlusDraw.setLayoutY(layoutY + 15);
        buttMinusDraw = new Button("�");
        buttMinusDraw.setTextFill(Color.RED);
        buttMinusDraw.setPrefWidth(25);
        buttMinusDraw.setLayoutX(layoutX + 180);
        buttMinusDraw.setLayoutY(layoutY + 15);
        buttPlusRow = new Button("+");
        buttPlusRow.setTextFill(Color.RED);
        buttPlusRow.setPrefWidth(25);
        buttPlusRow.setLayoutX(layoutX + 155);
        buttPlusRow.setLayoutY(layoutY + 80);
        buttMinusRow = new Button("�");
        buttMinusRow.setTextFill(Color.RED);
        buttMinusRow.setPrefWidth(25);
        buttMinusRow.setLayoutX(layoutX + 180);
        buttMinusRow.setLayoutY(layoutY + 80);
    }

    private void buttPlusDrawAction(ActionEvent e) {
        if (sliderDraws.getMax() > sliderDraws.getValue()) {
            sliderDraws.increment();
        }
    }

    private void buttMinusDrawAction(ActionEvent e) {
        if (sliderDraws.getMin() < sliderDraws.getValue()) {
            sliderDraws.decrement();
        }
    }

    private void buttPlusRowAction(ActionEvent e) {
        if (sliderRows.getMax() > sliderRows.getValue()) {
            sliderRows.increment();
        }
    }

    private void buttMinusRowAction(ActionEvent e) {
        if (sliderRows.getMin() < sliderRows.getValue()) {
            sliderRows.decrement();
        }
    }
}
