package com.model.tomaszm;

	//constant values for whole program project
		public enum CONSTANTS{
			MAIN_ROOT("/com/root/tomaszm/MainRoot.fxml", 0),
			WRITTEN_NUMBERS_ROOT("/com/root/tomaszm/WrittenNumbersRoot.fxml", 0),
			NUMBERS_TEST_SHEET_ROOT("/com/root/tomaszm/NumbersTestSheetRoot.fxml", 0),
			STOPER_ROOT("/com/root/tomaszm/Stoper.fxml", 0),
			COUNTDOWN_ROOT("/com/root/tomaszm/Countdown.fxml",0),
			SPOKEN_NUMBERS_ROOT("/com/root/tomaszm/SpokenNumbersRoot.fxml", 0),
			CARDS_ROOT("/com/root/tomaszm/CardsRoot.fxml", 0),
			CARDS_TABLE_ROOT("/com/root/tomaszm/CardsTable.fxml", 0),
			
			DESIGN_CSS("/com/root/tomaszm/Design.css", 0),
			
			APPLICATION_ICON("Icon.png",0),
			MAIN_ROOT_BACKGROUND("MainRootBackground.jpg", 0),
			SPOKEN_NUMBERS_BACKGROUND("SpokenNumbersBackground.jpg", 0),
			CARDS_TABLE_BACKGROUND("CardsTableBackground.jpg", 0),
			CARDS_INFO_BACKGROUND("CardsTableInfoBackground.jpg", 0),
			TAB_PANE_BACKGROUND("TabPaneBackground.jpg", 0),
			
			fadeOutMillis("", 700),
		    fadeInMillis("", 1200);
			
			public String directory;
			public int value;
			private CONSTANTS(String directory, int value){
				this.directory = directory;
				this.value = value;
			}
			
		}

