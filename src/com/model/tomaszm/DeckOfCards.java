package com.model.tomaszm;

import java.util.ArrayList;
import java.util.Random;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;

/*
 * each card card has its own final ID
 * CardPane also has ID of card in it. When Shuffling, we just move images and change ID of panes
 */
public class DeckOfCards {

    private CardPane[][] deck = new CardPane[14][4]; //BorderPane with ImageView. 

    public DeckOfCards() {
        for (CardValue cardValue : CardValue.values()) {
            for (CardSuit cardSuit : CardSuit.values()) {
                Card card = new Card(cardValue, cardSuit);
                deck[cardValue.value][cardSuit.value] = new CardPane(cardValue, cardSuit); //seting ID
                deck[cardValue.value][cardSuit.value].setCenter(card);
            }
        }
    }

    public boolean isRight(CardValue cardValue, CardSuit cardSuit) {
        CardPane cardPane = deck[cardValue.value][cardSuit.value];
        Card card = (Card) cardPane.getCenter();
        if (card == null) {
            return false;
        }

        boolean isValueRight = cardPane.getValueID() == card.getValue();
        boolean isSuitRight = cardPane.getSuitID() == card.getSuit();
        if (isValueRight && isSuitRight) {
            return true;
        } else {
            return false;
        }
    }

    public CardPane getCard(CardValue cardValue, CardSuit cardSuit) {
        return deck[cardValue.value][cardSuit.value];
    }

    public CardPane getCard(int cardValue, int cardSuit) {
        return deck[cardValue][cardSuit];
    }

    public void mixDeck() {
        //getting pack into list
        ArrayList<Card> newDeck = new ArrayList<>();
        for (CardValue cardValue : CardValue.values()) {
            for (CardSuit cardSuit : CardSuit.values()) {
                newDeck.add((Card) deck[cardValue.value][cardSuit.value].getCenter());
            }
        }

        Random randomize = new Random();
        for (CardValue cardValue : CardValue.values()) {
            for (CardSuit cardSuit : CardSuit.values()) {
                int next = randomize.nextInt(newDeck.size());
                Card card = newDeck.get(next);
                deck[cardValue.value][cardSuit.value].setCenter(card);
                deck[cardValue.value][cardSuit.value].setValueID(card.getValue());
                deck[cardValue.value][cardSuit.value].setSuitID(card.getSuit());
                newDeck.remove(next);
            }
        }

    }

    public void newUnshuffledDeck() {
        for (CardValue cardValue : CardValue.values()) {
            for (CardSuit cardSuit : CardSuit.values()) {
                Card nextCard = new Card(cardValue, cardSuit);
                deck[cardValue.value][cardSuit.value].setCenter(nextCard);
                deck[cardValue.value][cardSuit.value].setValueID(cardValue);
                deck[cardValue.value][cardSuit.value].setSuitID(cardSuit);
            }
        }
    }

    public void removeCards() {
        for (CardValue cardValue : CardValue.values()) {
            for (CardSuit cardSuit : CardSuit.values()) {
                deck[cardValue.value][cardSuit.value].setCenter(null);
            }
        }
    }

    public void setStyle(String style) {
        for (CardValue cardValue : CardValue.values()) {
            for (CardSuit cardSuit : CardSuit.values()) {
                deck[cardValue.value][cardSuit.value].setStyle(style);
            }
        }
    }

    public String getCardsLayoutOrder() {
        StringBuilder layout = new StringBuilder("");

        for (int i = 0, counter = 1; CardValue.values().length > i; i++) {
            for (int j = 0; CardSuit.values().length > j; j++, counter++) {
                String valueName = deck[i][j].getValueID().name;
                char suitName = deck[i][j].getSuitID().name;
                layout.append(valueName).append(suitName).append("; ");
                if (counter % 13 == 0) {
                    layout.append("\n");
                }
            }
        }
        return layout.toString();
    }

    public void makeCardPanesListening() {
        for (CardValue cardValue : CardValue.values()) {
            for (CardSuit cardSuit : CardSuit.values()) {
                CardPane cardPane = deck[cardValue.value][cardSuit.value];
                cardPane.setOnDragOver(event -> {
                    if (event.getGestureSource() instanceof Card) {
                        event.acceptTransferModes(TransferMode.ANY);
                    }
                });

                cardPane.setOnDragDropped(event -> {
                    if (event.getGestureSource() instanceof Card) {
                        if (cardPane.getCenter() != null) { // when target is not empty we need to switch cards
                            CardPane pane = (CardPane) ((Card) event.getGestureSource()).getParent();
                            pane.setCenter(cardPane.getCenter());
                        }
                        cardPane.setCenter((Card) event.getGestureSource());
                        event.setDropCompleted(true);
                    }
                });
            }
        }
    }

    public void makeCardsDraggable() { //only if CardPanes have cards inside
        for (CardValue cardValue : CardValue.values()) {
            for (CardSuit cardSuit : CardSuit.values()) {
                Card nextCard = (Card) deck[cardValue.value][cardSuit.value].getCenter();
                nextCard.setCursor(Cursor.HAND);

                nextCard.setOnDragDetected(event -> {
                    Dragboard db = nextCard.startDragAndDrop(TransferMode.MOVE);
                    ClipboardContent content = new ClipboardContent();
                    content.putImage(nextCard.getImage());
                    db.setContent(content);
                });
            }
        }
    }

    public void makeEnterExitEffect() { // color changing
        for (CardValue cardValue : CardValue.values()) {
            for (CardSuit cardSuit : CardSuit.values()) {
                CardPane cardPane = deck[cardValue.value][cardSuit.value];
                cardPane.setOnDragEntered(event -> {
                    if (event.getGestureSource() instanceof Card) {
                        cardPane.setStyle("-fx-background-color: radial-gradient( focus-angle 0.0deg, focus-distance 0.0%, center 50.0% 50.0%, radius 100.0%, rgba(255,255,255,0.8045976758003235) 0.0, rgba(193,6,6,0.6091954112052917) 100.0 )");
                    }
                });

                cardPane.setOnDragExited(event -> {
                    if (event.getGestureSource() instanceof Card) {
                        String oldCardPaneStyle = "-fx-border-color: black; -fx-border-width: 0.2px;"
                                + "-fx-background-color: radial-gradient( focus-angle 0.0deg, focus-distance 0.0%, center 50.0% 50.0%, radius 100.0%, rgba(255,255,255,0.16091954708099365) 0.0, rgba(153,179,255,0.2183908075094223) 100.0 )";
                        cardPane.setStyle(oldCardPaneStyle);
                    }
                });
            }
        }
    }

    public void showNameLabels() {
        for (CardValue cardValue : CardValue.values()) {
            for (CardSuit cardSuit : CardSuit.values()) {
                CardPane cardPane = deck[cardValue.value][cardSuit.value];

                String name = cardPane.getValueID().name + cardPane.getSuitID().name;

                if (cardPane.getBottom() == null) { //theres is no label yet
                    Label cardLabel = new Label();
                    if (isRight(cardValue, cardSuit)) {
                        cardLabel.setText(name + " - Correct");
                        cardLabel.setStyle("-fx-background-color: linear-gradient( from 31.0% 0.0% to 100.0% 100.0%, rgba(255,255,255,0.10) 0.0, rgba(53,236,36,0.5057471394538879) 100.0)");
                    } else {
                        cardLabel.setText(name + " - Wrong");
                        cardLabel.setStyle("-fx-background-color: linear-gradient( from 31.0% 100.0% to 100.0% 100.0%, rgba(255,255,255,0.10) 0.0, rgba(255,0,0,0.29885056614875793) 100.0)");
                    }

                    cardLabel.setPrefWidth(80);
                    cardLabel.setMaxHeight(10);
                    cardLabel.setAlignment(Pos.CENTER);
                    cardPane.setBottom(cardLabel);
                } else { //there is already a label, so we just check again if its correct
                    Label cardLabel = (Label) cardPane.getBottom();
                    if (isRight(cardValue, cardSuit)) {
                        cardLabel.setText(name + " - Correct");
                        cardLabel.setStyle("-fx-background-color: linear-gradient( from 31.0% 0.0% to 100.0% 100.0%, rgba(255,255,255,0.10) 0.0, rgba(53,236,36,0.5057471394538879) 100.0)");
                    } else {
                        cardLabel.setText(name + " - Wrong");
                        cardLabel.setStyle("-fx-background-color: linear-gradient( from 31.0% 100.0% to 100.0% 100.0%, rgba(255,255,255,0.10) 0.0, rgba(255,0,0,0.29885056614875793) 100.0)");
                    }
                }
            }
        }
    }

    public void removeLabels() {
        for (CardValue cardValue : CardValue.values()) {
            for (CardSuit cardSuit : CardSuit.values()) {
                if (deck[cardValue.value][cardSuit.value].getBottom() != null) {
                    deck[cardValue.value][cardSuit.value].setBottom(null);
                }
            }
        }
    }

    /**
     * ******* PRIVATE CLASSES ********************
     */
    public enum CardValue {
        ACE(0, "A"), TEN(1, "10"), TWO(2, "2"), THREE(3, "3"), FOUR(4, "4"), FIVE(5, "5"), SIX(6, "6"), SEVEN(7, "7"),
        EIGHT(8, "8"), NINE(9, "9"), JACK(10, "J"), QUEEN(11, "Q"), KING(12, "K");

        public final int value;
        public final String name;

        CardValue(int value, String name) {
            this.value = value;
            this.name = name;
        }
    }

    public enum CardSuit {
        DIAMONDS(0, 'D'), SPADES(1, 'S'), HEARTS(2, 'H'), CLUBS(3, 'C');

        public final int value;
        private final char name;

        CardSuit(int value, char name) {
            this.value = value;
            this.name = name;
        }
    }

    public class Card extends ImageView {

        private final CardValue cardValue;
        private final CardSuit cardSuit;

        public Card(CardValue cardValue, CardSuit cardSuit) {
            super();

            this.cardValue = cardValue;
            this.cardSuit = cardSuit;

            this.setImage(new Image(buildDirectory(cardValue, cardSuit)));
            this.setFitWidth(80);
            this.setFitHeight(90);
        }

        private String buildDirectory(CardValue cardValue, CardSuit cardSuit) {
            StringBuilder directory = new StringBuilder("/");

            switch (cardValue) {
                case ACE:
                    directory.append("A");
                    break;
                case TEN:
                    directory.append("10");
                    break;
                case TWO:
                    directory.append("2");
                    break;
                case THREE:
                    directory.append("3");
                    break;
                case FOUR:
                    directory.append("4");
                    break;
                case FIVE:
                    directory.append("5");
                    break;
                case SIX:
                    directory.append("6");
                    break;
                case SEVEN:
                    directory.append("7");
                    break;
                case EIGHT:
                    directory.append("8");
                    break;
                case NINE:
                    directory.append("9");
                    break;
                case JACK:
                    directory.append("J");
                    break;
                case QUEEN:
                    directory.append("Q");
                    break;
                case KING:
                    directory.append("K");
                    break;
                default:
                    directory.append("???");
                    break;
            }
            switch (cardSuit) {
                case DIAMONDS:
                    directory.append("D");
                    break;
                case SPADES:
                    directory.append("S");
                    break;
                case HEARTS:
                    directory.append("H");
                    break;
                case CLUBS:
                    directory.append("C");
                    break;
                default:
                    directory.append("???");
                    break;
            }

            directory.append(".jpg");

            return directory.toString();
        }

        public CardValue getValue() {
            return cardValue;
        }

        public CardSuit getSuit() {
            return cardSuit;
        }
    }

    public class CardPane extends BorderPane {

        private CardValue valueID;
        private CardSuit suitID;

        public CardPane(CardValue valueID, CardSuit suitID) {
            super();

            getStyleClass().add("cardPane");
            setMinSize(80, 90);

            this.valueID = valueID;
            this.suitID = suitID;
        }

        public CardValue getValueID() {
            return valueID;
        }

        public CardSuit getSuitID() {
            return suitID;
        }

        public void setValueID(CardValue valueID) {
            this.valueID = valueID;
        }

        public void setSuitID(CardSuit suitID) {
            this.suitID = suitID;
        }
    }
}
