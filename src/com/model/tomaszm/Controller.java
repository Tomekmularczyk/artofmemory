package com.model.tomaszm;

import javafx.scene.layout.Pane;

public abstract class Controller{  //we need this class to pass different controllers-class with different Panes to Main application -> changeRoot(...);
	
   public abstract Pane getMainPane();
   
}
