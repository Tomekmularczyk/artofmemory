package com.model.tomaszm;

import java.util.ArrayList;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

//*******************************************************************
public class MyData { //dont forget about public because you wont get acces to properties

    private ObservableList<ObjectProperty<Integer>> cellValue = FXCollections.observableArrayList();
    private ArrayList<Integer> corespondingValues = new ArrayList<Integer>();// wenn need it to match cell value with it and later
    // use it to make not valid cells diffrent color
    private static int numberOfRow = 0;

    public MyData(int howManyColumns) {
        this.cellValue.add(new SimpleObjectProperty<Integer>(numberOfRow + 1));//first column row number
        for (int i = 1; i < howManyColumns; ++i) {
            this.cellValue.add(new SimpleObjectProperty<Integer>(null));
        }

        ++numberOfRow;
    }

    public ObjectProperty<Integer> getCellValue(int whichOne) {
        return cellValue.get(whichOne);
    }

    public void setCellValue(ObjectProperty<Integer> cellValue, int whichOne) {
        this.cellValue.set(whichOne, cellValue);
    }

    public void addNew() { //ads another variable for another column
        cellValue.add(new SimpleObjectProperty<Integer>(null));
    }

    public void deleteLast() { //deletes last variable when column is deleted
        cellValue.remove(cellValue.size() - 1);
    }

    public static void clearNumberOfRow() {
        numberOfRow = 0;
    }

    public ObservableList<ObjectProperty<Integer>> returnCellsData() {
        return cellValue;
    }

    public ArrayList<Integer> setCorespondingValues(ArrayList<Integer> corespondingValues) {
        return this.corespondingValues = corespondingValues;
    }

    public Integer getCorespondingValue(int whichOne) {
        return corespondingValues.get(whichOne);
    }
}

		//*******************************************************************
