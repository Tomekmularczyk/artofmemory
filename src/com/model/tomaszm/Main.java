package com.model.tomaszm;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    public static Stage primaryStage;   //to get acces to our actual stage anywhere in a program
    public static Main mainApplication; //to get acces to non-static method changeRoot

    /**
     * ************** MAIN() AND START() *******************
     */
    @Override
    public void start(Stage primaryStage) {
        try {
            Main.primaryStage = primaryStage;
            mainApplication = this;

            Parent root = FXMLLoader.load(getClass().getResource(CONSTANTS.MAIN_ROOT.directory));
            primaryStage.setResizable(false);

            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource(CONSTANTS.DESIGN_CSS.directory).toExternalForm());

            primaryStage.getIcons().add(new Image(CONSTANTS.APPLICATION_ICON.directory));

            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
