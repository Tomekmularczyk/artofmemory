package com.model.tomaszm;

import java.util.ArrayList;
import java.util.Random;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.text.Font;

public class NumberGeneratorModel {

    private static int iNumberOfDraws = 5;
    private static Random randomize = new Random();
    private static String sPreffix = "";
    private static String sSuffix = "";
    private static boolean addZeros = true;
    private static boolean lineSpacing = false;
    private static boolean drawsSpacing = true;

    public static void setSpacing(boolean addZeros1, boolean lineSpacing1, boolean drawsSpacing1) {
        addZeros = addZeros1;
        lineSpacing = lineSpacing1;
        drawsSpacing = drawsSpacing1;
    }

    public static void setPreffixSuffix(String preffix, String suffix) {
        sPreffix = preffix;
        sSuffix = suffix;
    }

    public static boolean generateNumbers(TextField fieldMinV, TextField fieldMaxV, TextArea textArea, int iRows, int iDraws, boolean onlyOnce) {
        Integer iFrom, iTo;
        iNumberOfDraws = iDraws;

        boolean rowsAndDrawsAreOK = iRows > 0 && iDraws > 0;
        boolean onlyOnceAndDrawsOK = iDraws > 0 && onlyOnce;
        if (rowsAndDrawsAreOK || onlyOnceAndDrawsOK) {
            iFrom = convertFromString(fieldMinV.getText());
            iTo = convertFromString(fieldMaxV.getText());
            if (iFrom == null || iTo == null) {
                showTip(fieldMinV, "Wrong number format! Perhaps Max value is too big.");
                return false;
            }

            if (!isValidRange(iFrom, fieldMinV, iTo)) {
                return false;
            }

            //udalo sie...
            fillTextArea(iFrom, iTo, textArea, iRows, onlyOnce);
            return true;
        } else {
            return true;
        }
    }

    private static boolean isValidRange(Integer iFrom, TextField fieldMinV, Integer iTo) {
        if (iFrom <= -1) {
            showTip(fieldMinV, "Min value can't be less than 0!");
            return false;
        }
        if (iFrom >= iTo) {
            showTip(fieldMinV, "Min value should be less than Max value!");
            return false;
        }
        return true;
    }

    private static Integer convertFromString(String string) {
        try {
            return Integer.valueOf(string);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private static Point2D getNodePos(Node node) {  //getting node coordination on screen
        Scene nodeScene = node.getScene();
        final Point2D windowPos = new Point2D(nodeScene.getWindow().getX(), nodeScene.getWindow().getY());
        final Point2D scenePos = new Point2D(nodeScene.getX(), nodeScene.getY());
        final Point2D nodePos = node.localToScene(0.0, 0.0);
        final Point2D nodePosOnScreen = new Point2D(windowPos.getX() + scenePos.getX() + nodePos.getX(),
                windowPos.getY() + scenePos.getY() + nodePos.getY());
        return nodePosOnScreen;
    }

    private static void showTip(Node node, String text) {
        Point2D nodePosOnScreen = getNodePos(node);

        Tooltip tooltip = new Tooltip(text);
        tooltip.setFont(new Font(15));
        tooltip.setOpacity(0.8);
        tooltip.setAutoFix(true);
        tooltip.setAutoHide(true);
        tooltip.show(node, nodePosOnScreen.getX() - 30, nodePosOnScreen.getY() - 40);
    }

    public static int generate(int iMin, int iMax) {
        return randomize.nextInt((iMax - iMin) + 1) + iMin; // way of range random generating
    }

    private static void fillTextArea(int iMin, int iMax, TextArea textArea, int iRows, boolean eachNumOnlyOnce) {
        FillArea fillArea = new FillArea(iMin, iMax, textArea, iRows, eachNumOnlyOnce);
        Thread thread = new Thread(fillArea);
        thread.setDaemon(true);
        thread.start();

    }

    public static class FillArea implements Runnable {

        private int iMin, iMax, iRows;
        private TextArea textArea;
        private boolean eachNumOnlyOnce;
        private static ArrayList<EventHandler<ActionEvent>> listeners = new ArrayList<>();

        public FillArea(int iMin, int iMax, TextArea textArea, int iRows, boolean eachNumOnlyOnce) {
            this.iMin = iMin;
            this.iMax = iMax;
            this.iRows = iRows;
            this.textArea = textArea;
            this.eachNumOnlyOnce = eachNumOnlyOnce;
        }

        public static void setOnFinishAction(EventHandler<ActionEvent> listener) {
            listeners.add(listener);
        }

        @Override
        public void run() {
            StringBuilder text = new StringBuilder();

            //making all numberss even in length
            int iLenghtOfMax = String.valueOf(iMax).length();
            String sFormat = "%0" + iLenghtOfMax + "d";

            if (eachNumOnlyOnce) { 
                generateUniqueNumbers(text, sFormat);
            } else {
                generateDuplicatedNumbers(text, sFormat);
            }
            
            Platform.runLater(() -> {
                textArea.setText(text.toString());
                for (EventHandler<ActionEvent> l : listeners){ //calling finish
                    l.handle(new ActionEvent());
                }
            });
        }

        private void generateUniqueNumbers(StringBuilder text, String sFormat) {
            ArrayList<Integer> listOfNumbers = new ArrayList<>();
            for (int i = iMin; i <= iMax; i++) { 
                listOfNumbers.add(i);
            }
            
            boolean onlyOneLine = listOfNumbers.size() <= iNumberOfDraws;
            for (int i = 0; listOfNumbers.size() > 0 || (i < iNumberOfDraws && !onlyOneLine); ++i) {
                if (listOfNumbers.size() > 0) {
                    int nextElement = generate(0, listOfNumbers.size() - 1);
                    if (addZeros) {
                        text.append(sPreffix).append(String.format(sFormat, listOfNumbers.get(nextElement))).append(sSuffix);
                    } else {
                        text.append(sPreffix).append(Integer.toString(listOfNumbers.get(nextElement))).append(sSuffix);
                    }
                    listOfNumbers.remove(nextElement);
                } else if (addZeros) {// fillin with 0's to the end of line
                    text.append(sPreffix).append(String.format(sFormat, 0)).append(sSuffix);
                } else {
                    text.append(sPreffix).append(Integer.toString(0)).append(sSuffix);
                }
                
                if (drawsSpacing) {
                    text.append(" ");
                }
                
                if (i + 1 == iNumberOfDraws) {
                    text.append("\n");
                    if (listOfNumbers.size() > 0) {
                        i = -1;
                    } else {
                        ++i;
                    }
                }
                
                if (lineSpacing) {
                    text.append("\n");
                }
            }
        }

        private void generateDuplicatedNumbers(StringBuilder text, String sFormat) {
            for (int i = 0; i < iRows; i++) { // do for number of rows
                for (int j = 0; j < iNumberOfDraws; j++) { //do X draws
                    if (addZeros) {
                        text.append(sPreffix).append(String.format(sFormat, generate(iMin, iMax))).append(sSuffix);
                    } else {
                        text.append(sPreffix).append(Integer.toString(generate(iMin, iMax))).append(sSuffix);
                    }
                    if (drawsSpacing) {
                        text.append(" ");
                    }
                }
                text.append(lineSpacing ? "\n\n" : "\n");
            }
        }

    }

}
