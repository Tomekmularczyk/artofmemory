package com.model.tomaszm;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Callback;

public class NewTableView extends TableView<MyData> {

    private static boolean evaluate = false;
    private static int numberOfColumns;
    private final IntegerProperty numberOfEmptyCells = new SimpleIntegerProperty(0);
    private final IntegerProperty numberOfFilledCells = new SimpleIntegerProperty(0);
    private ObservableList<MyData> dataList = FXCollections.observableArrayList();

    public static void setEvaluate(boolean evaluated) {
        evaluate = evaluated;
    }

    public static boolean getEvaluate() {
        return evaluate;
    }

    public NewTableView() {
        this("");
    }

    public NewTableView(String style) {
        this.setItems(dataList);
        numberOfColumns = 0;
        dataList.clear();
        MyData.clearNumberOfRow();

        this.setStyle(style);
        this.getSelectionModel().setCellSelectionEnabled(true);
        this.setEditable(true);
    }

    private void addColumn() {
        int i = numberOfColumns;// thats the key for lambda expression. Unicate number for column to access its variable;
        if (dataList.size() > 0)//resizing each data object with new variable
        {
            for (MyData x : dataList) {
                x.addNew();
            }
        }
        TableColumn<MyData, Integer> newColumn = new TableColumn<>((numberOfColumns == 0) ? "#" : String.valueOf(numberOfColumns));
        newColumn.setCellValueFactory(cellData -> cellData.getValue().getCellValue(i));
        Callback<TableColumn<MyData, Integer>, TableCell<MyData, Integer>> cellFactoryInt = (TableColumn<MyData, Integer> p) -> new EditingCellNumbers(this);
        newColumn.setCellFactory(cellFactoryInt);

        newColumn.setPrefWidth(25);
        newColumn.setMinWidth(20);
        newColumn.setMaxWidth(40);
        newColumn.setSortable(false);
        if (numberOfColumns == 0) {
            newColumn.setEditable(false);
            newColumn.setPrefWidth(20);
            newColumn.setMaxWidth(30);
        }

        this.getColumns().add(newColumn);
        ++numberOfColumns;
    }

    private void addRow() {
        dataList.add(new MyData(numberOfColumns));
    }

    public void addRowsAndColumn(int columns, int rows) {
        numberOfEmptyCells.set((columns - 1) * rows);
        for (int i = 0; i < columns; ++i) {
            this.addColumn();
        }
        for (int i = 0; i < rows; ++i) {
            this.addRow();
        }
    }

    public ObservableList<MyData> getData() {
        return dataList;
    }

    public void setData(ObservableList<MyData> data) {
        dataList = data;
    }

    public IntegerProperty getNumOfEmptyCells() {
        return numberOfEmptyCells;
    }

    public IntegerProperty getNumOfFilledCells() {
        return numberOfFilledCells;
    }
    //******************Klasa ta pozwala na definiowania zachowania kom�rek, kt�re edytuje u�ytkownik*****************

    public class EditingCellNumbers extends TableCell<MyData, Integer> {

        private TextField textField;
        private TableView<MyData> parentTableView;

        public EditingCellNumbers(TableView<MyData> parent) {
            this.parentTableView = parent;
            INFO.tooltipCellValid.setOpacity(0.7);
            INFO.tooltipCellInvalid.setOpacity(0.7);
        }

        @Override
        public void startEdit() {
            if (!isEmpty() && (isEditable())) {
                super.startEdit();
                createTextField();
                setText(null);
                setGraphic(textField);
                textField.selectAll();
                textField.requestFocus();
                getTableRow();
            }
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();

            if (getItem() != null) {
                setText(String.valueOf(getItem()));
            } else {
                setText(null);

                commitEdit(null);
            }
            setGraphic(null);
        }

        @Override
        public void updateItem(Integer item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else if (isEditing()) {
                if (textField != null) {
                    textField.setText(getString());
                }
                setText(null);
                setGraphic(textField);
            } else {
                setText(getString());
                setGraphic(null);
                if (getTableColumn().getText() == "#") {
                    setStyle("-fx-font-weight: bold;"
                            + "-fx-background-color: linear-gradient( from -100.0% 150.0% to 120.0% 100.0%, rgb(128,128,128) 0.0, rgb(255,255,255) 100.0);");
                } else if (!evaluate) {
                    if (getItem() == null) {
                        setStyle("-fx-border-color: lavender; -fx-border-width: 0 1 0 0;");
                    } else {
                        setStyle("-fx-text-fill: blue; -fx-border-width: 0 1 1 0;");
                    }
                } else if (getItem() == null) {
                    setStyle("-fx-background-color: radial-gradient( focus-angle 0.0deg, focus-distance 0.0%, center 50.0% 50.0%, radius 100.0%, rgba(255,0,0,0.1034482792019844) 0.0, rgb(255,255,255) 20.0, rgb(255,255,255) 100.0 )");
                } // ----- we are matching vlue of cell with value in data
                else if (getTableColumn().getText() != "#") {
                    if (getItem() == getValueOfData(Integer.valueOf(getTableColumn().getText()), this.getIndex())) {
                        setStyle("-fx-border-color: lavender; -fx-border-width: 0 1 1 0; -fx-text-fill: black");
                        setTooltip(INFO.tooltipCellValid);
                        setEditable(false); //-- once is valid, we cant edit it(because what for?)
                    } else {
                        setTooltip(INFO.tooltipCellInvalid);
                        setStyle("-fx-text-fill: red");
                    }
                }
            }
        }

        private void createTextField() {
            textField = new TextField(getString());
            textField.setStyle("-fx-background-color: ivory; -fx-border-color: blue;");
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.focusedProperty().addListener(
                    (ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) -> {
                        if (!arg2) {
                            if (getItem() != null) {
                                try {
                                    commitEdit(Integer.valueOf(textField.getText()));
                                } catch (NumberFormatException f) {
                                    commitEdit(null);
                                }
                            } else {
                                commitEdit(null);
                            }
                        }
                    });
            textField.setOnKeyReleased(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent event) {
                    if (event.getCode() == KeyCode.BACK_SPACE) {
                        if (getItem() != null) {
                            numberOfEmptyCells.set(numberOfEmptyCells.get() + 1);
                            numberOfFilledCells.set(numberOfFilledCells.get() - 1);
                        }
                        commitEdit(null);
                        moveToNext(false);
                    } else {
                        try {
                            int i = Integer.valueOf(textField.getText());
                            //digit given...
                            if ((i >= 0) && (i < 10)) {//making sure cell is filled with just one digit
                                if (getItem() == null) {
                                    numberOfEmptyCells.set(numberOfEmptyCells.get() - 1);
                                    numberOfFilledCells.set(numberOfFilledCells.get() + 1);
                                }
                                commitEdit(Integer.valueOf(textField.getText()));
                                moveToNext(true);
                            } else {
                                textField.clear();
                            }
                        } catch (NumberFormatException e) {
                            textField.clear();
                        }
                    }
                }
            });
        }

        private void moveToNext(boolean next) {
            final int selectedColumn = parentTableView.getSelectionModel().getSelectedCells().get(0).getColumn(); // gets the number of selected column
            final int selectedRow = parentTableView.getSelectionModel().getSelectedCells().get(0).getRow();

            if (next) {       //moving to next cell editing
                if (selectedColumn < numberOfColumns - 1) {
                    parentTableView.getSelectionModel().selectNext();
                    parentTableView.edit(selectedRow, parentTableView.getColumns().get(selectedColumn + 1));
                    parentTableView.scrollToColumnIndex(selectedColumn + 1);
                } else {
                    parentTableView.getSelectionModel().select(selectedRow + 1, parentTableView.getColumns().get(1));
                    parentTableView.edit(selectedRow + 1, parentTableView.getColumns().get(1));
                    if (!((selectedRow == dataList.size() - 1) && (selectedColumn == numberOfColumns - 1))) //not last cell
                    {
                        parentTableView.scrollToColumnIndex(1);
                    }
                    parentTableView.scrollTo(selectedRow);
                }
            } else if (selectedColumn > 1) {//moving to previous cell
                parentTableView.getSelectionModel().selectPrevious();
                parentTableView.edit(selectedRow, parentTableView.getColumns().get(selectedColumn - 1));
                parentTableView.scrollToColumnIndex(selectedColumn - 1);
            } else {
                parentTableView.getSelectionModel().select(selectedRow - 1, parentTableView.getColumns().get(numberOfColumns - 1));
                parentTableView.edit(selectedRow - 1, parentTableView.getColumns().get(numberOfColumns - 1));
                if (!((selectedRow == 0) && (selectedColumn == 1))) // not first cell
                {
                    parentTableView.scrollToColumnIndex(numberOfColumns - 1);
                }
                parentTableView.scrollTo(selectedRow - 2);
            }
        }

        private int getValueOfData(int xColumn, int yRow) {
            ObservableList<MyData> data = ((NewTableView) parentTableView).getData();
            return data.get(yRow).getCorespondingValue(xColumn - 1);
        }

        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }

}
