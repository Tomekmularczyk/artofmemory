package com.model.tomaszm;

import java.awt.Desktop;
import java.net.URL;
import javafx.scene.Parent;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.VBox;

public final class INFO {

    public static Tooltip tooltipSuffix = new Tooltip("this text will be added to the end of each draw for ex. \"44xx\"");
    public static Tooltip tooltipPreffix = new Tooltip("this text will be added to the begining of each draw for ex. \"xx44\"");
    public static Tooltip tooltipSliderDraws = new Tooltip("how many numbers you want to be generted in a single line?");
    public static Tooltip tooltipEachNumOnlyOnce = new Tooltip("each lines has to be equal so when needed additional 0's will be added");
    public static Tooltip tooltipButtGenerate = new Tooltip("fill the area based on current options setting");
    public static Tooltip tooltipCellInvalid = new Tooltip("double click to edit");
    public static Tooltip tooltipCellValid = new Tooltip("this value is correct");
    public static Tooltip tooltipDigitsInfo = new Tooltip("total number of digits");
    public static Tooltip tooltipDrawsInfo = new Tooltip("total count of generated numbers");
    public static Tooltip tooltipInterval = new Tooltip("time space between each digit");
    public static Tooltip tooltipCountdown = new Tooltip("how many digits you want to be said?");

    //#################################################################################
    private final static String GENERAL_INFO
            = "Hi!\n"
            + "Purpose of this program is to help you train your memory at World Championship level.\n"
            + "Right now program offers posibility of memorizing digits, spoken numbers and cards.\n"
            + "Each feature include many ways to help you get to the level you want. You can always\n"
            + "click help menu to get more details about particular feature, or often if you just\n"
            + "ride the mouse on certain button or field, you will get short information about it.\n\n"
            + "If you want to contact me, report of any problems or leave a comment go to: \n";
    private final static String GENERAL_INFO2 = "\nOther resources you might find usefull: \n";
    private final static String linkMySite = "http://tomekmularczyk.weebly.com/programming/art-of-mem";
    private final static String linkArtOfMemory = "http://artofmemory.com";
    private final static String linkMemoryStatistics = "http://world-memory-statistics.com/home.php";
    private final static String linkRecordHolders = "http://recordholders.org/en/list/memory.html";

    public static final Parent getGeneralInfoPane() {
        VBox vbox = new VBox();

        Label labMainInfo1 = new Label(GENERAL_INFO);
        Label labMainInfo2 = new Label(GENERAL_INFO2);
        Hyperlink hyperMySite = new Hyperlink(linkMySite);
        hyperMySite.setOnAction(e -> {
            openWebpage(linkMySite);
        });
        Hyperlink hyperArtOfMemory = new Hyperlink(linkArtOfMemory);
        hyperArtOfMemory.setOnAction(e -> {
            openWebpage(linkArtOfMemory);
        });
        Hyperlink hyperMemoryStatistics = new Hyperlink(linkMemoryStatistics);
        hyperMemoryStatistics.setOnAction(e -> {
            openWebpage(linkMemoryStatistics);
        });
        Hyperlink hyperRecordHolders = new Hyperlink(linkRecordHolders);
        hyperRecordHolders.setOnAction(e -> {
            openWebpage(linkRecordHolders);
        });

        vbox.getChildren().addAll(labMainInfo1, hyperMySite, labMainInfo2, hyperArtOfMemory, hyperMemoryStatistics, hyperRecordHolders);

        return vbox;
    }

    public static void openWebpage(String urlString) {
        try {
            Desktop.getDesktop().browse(new URL(urlString).toURI());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //#################################################################################
    private final static String WRITTEN_NUMBERS_INFO = "This section covers numbers generating feature.\n\n"
            + "Generate binary numbers, set a range within\n"
            + "numbers should be generated or mark in settings\n"
            + "menu that you want each number to be used only\n"
            + "once(to match the number of draws you want in each\n"
            + "line, adequate number of 0's will be added(re-\n"
            + "member about it when you go to Test Sheet feature)).\n\n"
            + "You can also add preffix and suffix text so you might\n"
            + "generate numbers with text like \"xx88\" or \"44+\",\n"
            + "however only digits from this strings will be later\n"
            + "validated\n\n"
            + "You can generate numbers, save them, or set the\n"
            + "timer and test yourself!\n\n"
            + "In the bottom of window there are few usefull\n"
            + "informations about project you may want to know.\n\n"
            + "Many times when you are not sure you can just ride a\n"
            + "the mouse on top of an element and you will recieve\n"
            + "a hint what it might do.";

    public static final Parent getWrittenNumbersInfo() {
        TextArea textArea = new TextArea(WRITTEN_NUMBERS_INFO);
        textArea.setEditable(false);
        textArea.setPrefSize(333, 210);

        return textArea;
    }

    //#################################################################################
    private final static String NUMBERS_TEST_SHEET_INFO = "This section deals with evaluating your memory.\n\n"
            + "Double click the table and write what you managed to\n"
            + "memorize. Digits wont be evaluated untill you press\n"
            + "\"Check\" option from menu \"Options\". Once you\n"
            + "press it, numbers that you put into table will\n"
            + "be validated with earlier generated numbers.\n\n"
            + "Also after clicking \"Check\" option from menu, the test\n"
            + "mode will be disabled and each number you put to\n"
            + "the table will be instantly evaluated. Red digits are\n"
            + "places where you gone wrong.\n\n"
            + "You can also check what numbers you have forgotten\n"
            + "by clicking at \"Source digits\" menu item from\n"
            + "menu \"Options\".";

    public static final Parent getTestSheetInfo() {
        TextArea textArea = new TextArea(NUMBERS_TEST_SHEET_INFO);
        textArea.setEditable(false);
        textArea.setPrefSize(333, 210);

        return textArea;
    }

    //#################################################################################
    private final static String SPOKEN_NUMBERS_INFO = "Try your memory when it comes to remembering\n"
            + "spoken numbers!\n\n"
            + "You can set how many digits you want to be\n"
            + "said, choose time interval between each digit\n"
            + "and also set extra break(for ex. 10 seconds\n"
            + "of brake after 40 digits). You can also set\n"
            + "the program to tell only binary numbers.\n\n"
            + "When you are ready go to test sheet by\n"
            + "clicking menu \"Try Me!\" and validate\n"
            + "what you have previously remembered.\n\n"
            + "If you stop the pronouncing program will keep\n"
            + "working until current cycle of 10 digits\n"
            + "is over.\n\n"
            + "Feature support currently 4 different\n"
            + "languages: Polish, English, Chinesse and\n"
            + "Deutsch.\n\n"
            + "For best user experience suggested is not\n"
            + "to actively use program when numbers are\n"
            + "being pronounced.";

    public static final Parent getSpokenNumbersInfo() {
        TextArea textArea = new TextArea(SPOKEN_NUMBERS_INFO);
        textArea.setEditable(false);
        textArea.setPrefSize(300, 210);

        return textArea;
    }
    //#################################################################################
    private final static String CARDS_INFO = "Test your memory when it comes to cards!\n"
            + "This feature allows you to create many decks of\n"
            + "52 cards, shuffle them, memorize and later test\n"
            + "how did you do.\n\n"
            + "Set the timer if you want to check how will you\n"
            + "do under time pressure.\n\n"
            + "When you're finished memorizing test yourself by\n"
            + "clicking \"Try Me!\" in \"Options\" menu. It's\n"
            + "really easy, each deck that was  previously created\n"
            + "is removed from the table and you get new unshuffled\n"
            + "deck in bottom table. Just drag cards to the same\n"
            + "place they were layed previously.\n\n"
            + "\"Source file\" option form \"Options\" menu allow you\n"
            + "to see layout of cards that were previously deleted.\n"
            + "Use only when you have to!\n\n"
            + "Once you're ready click \"Check\" menu from \"Options\"\n"
            + "menu and test mode will be terminated.\n"
            + "Program will check each deck and card for you.\n"
            + "Also will print how many cards you get right and\n"
            + "stick a label under each spot with a name of card\n"
            + "that should be there.\n\n"
            + "When everything is over click \"Start again\" from\n"
            + "\"Options\" menu to start memorizing again.\n";

    public static final Parent getCardsInfo() {
        TextArea textArea = new TextArea(CARDS_INFO);
        textArea.setEditable(false);
        textArea.setPrefSize(315, 210);

        return textArea;
    }

}
