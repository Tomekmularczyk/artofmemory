/**
 * You need:
 * 1. root to FXML file
 * 2. FXML file with set path to controller class
 * 3. FXML file controller class has to extend Controller class and override getMainPane() method
 * 4. If he wants to fade the stage and not change the window size, he still need to put controller class
 *
 * example:
 * ChangeTheRoot change = new ChangeTheRoot();
 * change.initialize(Main.primaryStage, Main.NUMBER_GENERATOR_ROOT, "Number Generator", true, new NumberGeneratorController());
 * change.doFadingTransition(stackPane, Main.fadeOutMillis, Main.fadeInMillis, true);
 *
 * 1. First user have declare object and construct it with initialize() method
 * Then if initialize() didnt return false(error) he may use two methods to change the root
 * - changeRoot(); simply changes the root in window
 * - doFadingTransition(); changes the root and also do the fading effect
 *
 * 2. if he wants to change the Stage while changing a root then he has to use constructor with two additional parameters:
 * - boolean bChangeStageSize - needs to be true
 * - Controller controller - the mother class of fxml file-controllers(each controller overrides getMainPane() method. We need it to
 * adjust Stage to new root size.
 *
 * 3. If he wants allso to fade the Stage and not only Scene then he has to use constructor  with additional parameters
 */
package com.model.tomaszm;

import java.io.IOException;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.controlsfx.dialog.Dialogs;

public class ChangeTheRoot { //class, so we can pass these methods to fadeOut or fadeIn

    private String sNewTitle;
    private boolean bChangeStageSize;
    private boolean bFadeStage = false; //we need to set manually if we want stage to fade
    private boolean bResizable;
    private Controller controller;
    private Stage primaryStage;
    private FXMLLoader loader;
    private Parent root;

    /**
     * *************CUSTOM MANUAL CONSTRUCTORS *******************
     */
    public boolean initialize(Stage primaryStage, boolean bResizable, String sRoot, String sNewTitle) {
        return initialize(primaryStage, bResizable, sRoot, sNewTitle, false);
    }

    public boolean initialize(Stage primaryStage, boolean bResizable, String sRoot, String sNewTitle, boolean bChangeStageSize) {
        this.primaryStage = primaryStage;
        this.bResizable = bResizable;
        this.sNewTitle = sNewTitle;
        this.bChangeStageSize = bChangeStageSize;

        loader = new FXMLLoader(getClass().getResource(sRoot)); //we load the root, so it is reachable in whole class

        try {
            root = loader.load();
            controller = loader.getController();
        } catch (IOException exception) {
            System.out.println(exception.getMessage());
            return false;
        }

        return true;
    }

    /**
     * ************************** PUBLIC METHODS ******************************
     */
    public Controller getController() {
        return controller;
    }

    public void doFadingTransition(Pane actualPane, double fadeOutMillis, double fadeInMillis) {
        doFadingTransition(actualPane, fadeOutMillis, fadeInMillis, false);
    }

    public void doFadingTransition(Pane actualPane, double fadeOutMillis, double fadeInMillis, boolean fadeTheStage) {
        bFadeStage = fadeTheStage;
        actualPane.setDisable(true);
        //fading out
        final DoubleProperty stageOpacity = Main.primaryStage.opacityProperty();
        final DoubleProperty oldPaneOpacity = actualPane.opacityProperty();
        if (bFadeStage) {
            oldPaneOpacity.bindBidirectional(stageOpacity);
        }

        Timeline fadeOut = createFadeOutTransition(oldPaneOpacity, fadeOutMillis, stageOpacity, fadeInMillis);
        fadeOut.play();
    }

    private Timeline createFadeOutTransition(final DoubleProperty oldPaneOpacity, double fadeOutMillis, final DoubleProperty stageOpacity, double fadeInMillis) {
        Timeline fadeOut = new Timeline(
                new KeyFrame(javafx.util.Duration.ZERO, new KeyValue(oldPaneOpacity, 1.0)),
                new KeyFrame(new javafx.util.Duration(fadeOutMillis), new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) { //what to do after end of fade-out
                        if (bResizable == true) {
                            Main.primaryStage.setResizable(true);
                        } else {
                            Main.primaryStage.setResizable(false);
                        }
                        if (bFadeStage) {
                            oldPaneOpacity.unbindBidirectional(stageOpacity);
                        }
                        changeRootAndWindow();
                        fadeIn(fadeInMillis);
                    }
                }, new KeyValue(oldPaneOpacity, 0.0)
                ));
        return fadeOut;
    }

    public final void changeRoot() {
        changeRootAndWindow();
    }

    /**
     * ************************** PRIVATE METHODS ******************************
     */
    private final void changeRootAndWindow() { //it will do depending what was put to the costructor
        if (primaryStage.isMaximized()) {
            primaryStage.setMaximized(false);
        }
        primaryStage.getScene().setRoot(root);
        primaryStage.setTitle(sNewTitle);

        if (bChangeStageSize) {
            Main.primaryStage.setHeight(controller.getMainPane().getPrefHeight() + 38);
            Main.primaryStage.setWidth(controller.getMainPane().getPrefWidth() + 16);
        }
    }

    private void fadeIn(double fadeInMillis) {
        if (controller == null) {
            Dialogs.create().title("Exception").masthead(null).message("insert controller class!").showInformation();
        }

        final DoubleProperty newPaneOpacity = controller.getMainPane().opacityProperty();
        final DoubleProperty stageOpacity = Main.primaryStage.opacityProperty();
        if (bFadeStage) {
            newPaneOpacity.bindBidirectional(stageOpacity);
        }

        Timeline fadeIn = createFadeInTransition(newPaneOpacity, fadeInMillis, stageOpacity);
        fadeIn.play();
    }

    private Timeline createFadeInTransition(final DoubleProperty newPaneOpacity, double fadeInMillis, final DoubleProperty stageOpacity) {
        Timeline fadeIn = new Timeline(
                new KeyFrame(javafx.util.Duration.ZERO, new KeyValue(newPaneOpacity, 0.0)),
                new KeyFrame(new javafx.util.Duration(fadeInMillis), new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        if (bFadeStage == true) {
                            newPaneOpacity.unbindBidirectional(stageOpacity);
                        }
                    }
                }, new KeyValue(newPaneOpacity, 1.0)
                ));
        return fadeIn;
    }
}
